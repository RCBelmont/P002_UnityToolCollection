﻿//*****************************************************************************
//Created By ZJ on 2018/6/1.
//
//@Description 摄像机移动操作 与 CameraMoveAdvEditor.cs编辑器代码配套
//*****************************************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveAdv : MonoBehaviour
{
    //移动轴类型
    public enum Axis
    {
        x = 0, //世界坐标X轴
        y = 1, //世界坐标Y轴
        z = 2, //世界坐标Z轴
        localX = 3, //本地坐标X轴
        localY = 4, //本地坐标Y轴
        localZ = 5 //本地坐标Z轴
    }

    //相机操作类型
    public enum MoveType
    {
        move = 0, //平移
        lookAround = 1, //环视
        rotate = 2 //旋转
    }

    //相机操作类型
    public MoveType CamMoveType = MoveType.move;

    //横向拖动操作对应的平移轴
    public Axis TouchY = Axis.localY;

    //纵向拖动操作对应的平移轴
    public Axis TouchX = Axis.localX;

    //横向拖动操作对应的最小阈值（根据操作类型不同包含多种意义，例如最小旋转角度，或者平移最小距离）
    public float MinOffsetX = -50;

    //纵向拖动操作对应的最小阈值（根据操作类型不同包含多种意义，例如最小旋转角度，或者平移最小距离）
    public float MinOffsetY = -50;

    //横向拖动操作对应的最大阈值（根据操作类型不同包含多种意义，例如最大旋转角度，或者平移最大距离）
    public float MaxOffsetX = 50;

    //纵向拖动操作对应的最大阈值（根据操作类型不同包含多种意义，例如最大旋转角度，或者平移最大距离）
    public float MaxOffsetY = 50;

    //缩放操作最小阈值
    public float MinZoom = -50;

    //缩放操作最大阈值
    public float MaxZoom = 50;

    //横向拖动操作倍率
    public float BoostX = 2;

    //纵向拖动操作倍率
    public float BoostY = 2;

    //缩放操作倍率
    public float BoostZoom = 5;

    //允许缩放操作(手机对应双指缩放手势，电脑对应鼠标滚轮）
    public bool AllowZoom = true;

    //允许横向拖动操作输入
    public bool AllowTouchX = true;

    //允许纵向拖动操作输入
    public bool AllowTouchY = true;

    //环视中心
    public GameObject LookCenter;

    //鼠标模式，在windows端和editor模式下开启，其他情况默认false
    private bool _mouseMode = false;

    //累计值与阈值配合用作移动返回限制
    //横向拖动操作累积值
    private float _offsetX = 0;

    //纵向拖动操作累积值
    private float _offsetY = 0;

    //缩放操作累积值
    private float _offsetZoom = 0;

    //触摸点距离
    private float _touchDistance = 0;

    //平衡系数，用于调整移动端和电脑端输入精度不同导致的速度差异
    private const float TOUCH_SCALE = 0.05f; //触摸系数
    private const float ZOMME_SCALE = 0.03f; //缩放系数

    private const float ZOMME_SCALE_MOUSE = 10; //鼠标缩放（滚轮）系数

    // Use this for initialization
    void Start()
    {
        //开启鼠标模式
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        _mouseMode = true;
#endif
        //环视模式下若无环视中心则会自行创建一个处于世界坐标原点的物体作为中心
        if (CamMoveType == MoveType.lookAround)
        {
            if (LookCenter == null)
            {
                LookCenter = new GameObject();
                LookCenter.name = "LOOK_CENTER";
            }

            this.transform.LookAt(LookCenter.transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_mouseMode)
        {
            MouseOptBase();
        }
        else
        {
            TouchOptBase();
        }
    }

    //触摸操作处理
    void TouchOptBase()
    {
        if (Input.touchCount == 1)
        {
            float inputX = AllowTouchX ? -Input.GetTouch(0).deltaPosition.x * TOUCH_SCALE : 0;
            float inputY = AllowTouchY ? -Input.GetTouch(0).deltaPosition.y * TOUCH_SCALE : 0;
            switch (CamMoveType)
            {
                case MoveType.move:
                    DoCameraMove(inputX, inputY);
                    break;
                case MoveType.lookAround:
                    DoCamLookAround(inputX, inputY);
                    break;
                case MoveType.rotate:
                    DoCamRotate(inputX, inputY);
                    break;
            }
        }

        //Deal Zoom
        if (AllowZoom)
        {
            if (CamMoveType != MoveType.move || (TouchX != Axis.localZ && TouchY != Axis.localZ))
            {
                DoCameraZoom(GetZoomGesture() * ZOMME_SCALE);
            }
        }
    }

    //鼠标操作处理
    void MouseOptBase()
    {
        float inputX = AllowTouchX ? -Input.GetAxis("Mouse X") : 0;
        float inputY = AllowTouchY ? -Input.GetAxis("Mouse Y") : 0;
        if (Input.GetMouseButton(0))
        {
            switch (CamMoveType)
            {
                case MoveType.move:
                    DoCameraMove(inputX, inputY);
                    break;
                case MoveType.lookAround:
                    DoCamLookAround(inputX, inputY);
                    break;
                case MoveType.rotate:
                    DoCamRotate(inputX, inputY);
                    break;
            }
        }

        //Deal Zoom

        if (AllowZoom)
        {
            if (CamMoveType != MoveType.move || (TouchX != Axis.localZ && TouchY != Axis.localZ))
            {
                DoCameraZoom(Input.GetAxis("Mouse ScrollWheel") * ZOMME_SCALE_MOUSE);
            }
        }
    }

    //处理相机环视
    void DoCamLookAround(float deltaX, float deltaY)
    {
        Vector3 axisX = Vector3.up;
        Vector3 axisY = Vector3.right;
        deltaX *= -BoostX;
        deltaY *= BoostY;
        float trueDeltaX = FixDelta(_offsetX, deltaX, MinOffsetX, MaxOffsetX);
        float trueDeltaY = FixDelta(_offsetY, deltaY, MinOffsetY, MaxOffsetY);
        this.transform.RotateAround(LookCenter.transform.position, Vector3.up, trueDeltaX);
        this.transform.RotateAround(LookCenter.transform.position, this.transform.right, trueDeltaY);
        this.transform.LookAt(LookCenter.transform.position);
        _offsetX += trueDeltaX;
        _offsetY += trueDeltaY;
    }

    //处理相机旋转
    void DoCamRotate(float deltaX, float deltaY)
    {
        Vector3 axisX = Vector3.up;
        Vector3 axisY = Vector3.right;
        deltaX *= BoostX;
        deltaY *= -BoostY;
        float trueDeltaX = FixDelta(_offsetX, deltaX, MinOffsetX, MaxOffsetX);
        float trueDeltaY = FixDelta(_offsetY, deltaY, MinOffsetY, MaxOffsetY);
        this.transform.Rotate(axisX, trueDeltaX, Space.World);
        this.transform.Rotate(axisY, trueDeltaY, Space.Self);
        _offsetX += trueDeltaX;
        _offsetY += trueDeltaY;
    }

    //处理相机平移
    void DoCameraMove(float deltaX, float deltaY)
    {
        Vector3 axisX = GetAixs(TouchX);
        Vector3 axisY = GetAixs(TouchY);
        deltaX = deltaX * BoostX;
        deltaY = deltaY * BoostY;
        float trueDeltaX = FixDelta(_offsetX, deltaX, MinOffsetX, MaxOffsetX);
        float trueDeltaY = FixDelta(_offsetY, deltaY, MinOffsetY, MaxOffsetY);
        this.transform.position += axisX * trueDeltaX + axisY * trueDeltaY;
        _offsetX += trueDeltaX;
        _offsetY += trueDeltaY;
    }

    //处理相机缩放
    //原理为相机在其自身Z轴方向移动，因此若平移选取了自身的Z轴则不会生效
    void DoCameraZoom(float deltaZoom)
    {
        Vector3 axisZoom = this.transform.forward;
        deltaZoom = deltaZoom * BoostZoom;
        float trueDeltaZoom = FixDelta(_offsetZoom, deltaZoom, MinZoom, MaxZoom);
        this.transform.position += axisZoom * trueDeltaZoom;
        _offsetZoom += trueDeltaZoom;
    }

    //根据选择的坐标轴类型获取坐标轴方向向量
    Vector3 GetAixs(Axis type)
    {
        switch (type)
        {
            case Axis.x:
                return Vector3.right.normalized;
            case Axis.y:
                return Vector3.up.normalized;
            case Axis.z:
                return Vector3.forward.normalized;
            case Axis.localX:
                return this.transform.right.normalized;
            case Axis.localY:
                return this.transform.up.normalized;
            case Axis.localZ:
                return this.transform.forward.normalized;
        }

        return Vector3.zero;
    }

    //计算缩放手势的变化
    float GetZoomGesture()
    {
        if (Input.touchCount != 2)
        {
            return 0;
        }

        if (Input.GetTouch(1).phase == TouchPhase.Began)
        {
            _touchDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
        }

        float newDistance = _touchDistance;
        float oldDistance = _touchDistance;

        if (Input.GetTouch(1).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            newDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
        }

        _touchDistance = newDistance;

        return newDistance - oldDistance;
    }

    //修正变化值
    float FixDelta(float oldVale, float delta, float min, float max)
    {
        float retDelta = 0;
        float newValue = oldVale + delta;
        //修正变化值
        if (newValue > max)
        {
            retDelta = max - oldVale;
        }
        else if (newValue < min)
        {
            retDelta = min - oldVale;
        }
        else
        {
            retDelta = delta;
        }

        return retDelta;
    }
}