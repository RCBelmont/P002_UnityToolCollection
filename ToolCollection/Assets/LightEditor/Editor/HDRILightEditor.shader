﻿//HDRILightEditor辅助shader
//CreateBy：RCBelmont
Shader "Hidden/HDRILightEditor"
{
    Properties
    {
        [NoScaleOffset] _MainTex ("Texture", Any) = "gray" { }
    }
    
    SubShader
    {
        Tags { "ForceSupported" = "true" }
        
        ZTest Always
        ZWrite Off
        Cull Off
        
        CGINCLUDE
        #include "UnityCG.cginc"
        #include "UnityShaderVariables.cginc"
        
        struct appdata
        {
            float4 vertex: POSITION;
            float2 uv: TEXCOORD0;
        };
        
        struct v2f
        {
            float2 uv: TEXCOORD0;
            float4 vertex: SV_POSITION;
            float3 vertexLocal: TEXCOORD1;
        };
        
        samplerCUBE _MainTex;
        sampler _MainTex1;
        half4 _MainTex_HDR;
        float4 _Size;
        float4 _ColorTint;
        float _Intensity;
        
        sampler2D _FrontTex;
        sampler2D _BackTex;
        sampler2D _LeftTex;
        sampler2D _RightTex;
        sampler2D _UpTex;
        sampler2D _DownTex;
        
        float3 RotateAroundYInDegrees(float3 vertex, float degrees)
        {
            float alpha = degrees * UNITY_PI / 180.0;
            float sina, cosa;
            sincos(alpha, sina, cosa);
            float2x2 m = float2x2(cosa, -sina, sina, cosa);
            return float3(mul(m, vertex.xz), vertex.y).xzy;
        }
        
        v2f vert(appdata v)
        {
            v2f o;
            
            o.uv = v.uv;
            
            o.vertex = UnityObjectToClipPos(v.vertex);
            o.vertexLocal = half3(v.vertex.x / _Size.x * 2 - 1, v.vertex.y / _Size.y * 2 - 1, 1);
            
            return o;
        }
        
        half4 frag(v2f i): SV_Target
        {
            
            half3 dir = normalize(float3(sin(i.vertexLocal.x * 3.14), tan(i.vertexLocal.y * 3.14 / 2), cos(i.vertexLocal.x * 3.14)));
            half4 col = texCUBElod(_MainTex, half4(dir, 1));
            //return (half4)vertexLocal.z;
            half3 c = DecodeHDR(col, _MainTex_HDR);
            c = c * unity_ColorSpaceDouble.rgb * _ColorTint;
            //return half4(0,1,1,1) ;
            return half4(c.rgb * _Intensity, 1);
        }
        
        half4 frag1(v2f i): SV_Target
        {
            
            half4 col = tex2D(_MainTex1, i.uv);
            half3 color = col.r * _ColorTint + col.g * (1 - step(1.5, _ColorTint.r + _ColorTint.g + _ColorTint.b)) * half3(1, 1, 1);
            
            return half4(color, col.a);
        }
        
        float4 sampleCube(float3 dir, float3 dir1)
        {
            dir = normalize(dir);
            dir = dir1 = normalize(dir1);
            float3 dirz = float3(0, 0, 1);
            float3 dirx = float3(1, 0, 0);
            float3 diry = float3(0, 1, 0);
            float ndx = dot(dir, dirx);
            float ndy = dot(dir1, diry);
            float ndz = dot(dir, dirz);
            float gamma = cos(3.14 / 4);
            float2 uv;
            //z
            uv = float2(dir.x / dir.z, dir.y / dir.z);
            uv = (uv + 1) / 2;
            if (uv.x < 1 && uv.x > 0 && uv.y > 0 && uv.y < 1 && ndz > 0)
            {
                return tex2D(_FrontTex, uv);
            }
            //-z
            uv = float2(dir.x / dir.z, -dir.y / dir.z);
            uv = (uv + 1) / 2;
            if (uv.x < 1 && uv.x > 0 && uv.y > 0 && uv.y < 1 && ndz < 0)
            {
                
                return tex2D(_BackTex, uv);
            }
            
            
            //x
            uv = float2(-dir.z / dir.x, dir.y / dir.x);
            uv = (uv + 1) / 2;
            if (uv.x < 1 && uv.x > 0 && uv.y > 0 && uv.y < 1 && ndx > 0)
            {
                return tex2D(_LeftTex, uv);
            }
            //-x
            uv = float2(-dir.z / dir.x, -dir.y / dir.x);
            uv = (uv + 1) / 2;
            if (uv.x < 1 && uv.x > 0 && uv.y > 0 && uv.y < 1 && ndx < 0)
            {
                return tex2D(_RightTex, uv);
            }
            
            //y
            uv = float2(dir1.x / dir1.y, -dir1.z / dir1.y);
            uv = (uv + 1) / 2;
            if (uv.x < 1 && uv.x > 0 && uv.y > 0 && uv.y < 1 && ndy > 0)
            {
                return tex2D(_UpTex, uv);
            }
            //-y
            uv = float2(-dir1.x / dir1.y, -dir.z / dir1.y);
            uv = (uv + 1) / 2;
            if (uv.x < 1 && uv.x > 0 && uv.y > 0 && uv.y < 1 && ndy < 0)
            {
                return tex2D(_DownTex, uv);
            }
            
            return half4(0, 0, 0, 0);
        }
        
        half4 frag2(v2f i): SV_Target
        {
            float alpha = i.vertexLocal.x * 3.14;
            float beta = i.vertexLocal.y * 3.14 / 2;
            float y = sin(beta);
            float3 pos = RotateAroundYInDegrees(float3(0, y, sqrt(1 - y * y)), i.vertexLocal.x * 180);
            float3 pos1 = float3(sin(i.vertexLocal.x * 3.14), tan(i.vertexLocal.y * 3.14 / 2), cos(i.vertexLocal.x * 3.14));
            half4 col = sampleCube(pos, pos1);
            half3 c = DecodeHDR(col, _MainTex_HDR);
            c = c * unity_ColorSpaceDouble.rgb * _ColorTint;
            return half4(c.rgb * _Intensity, 1);
        }
        ENDCG
        
        //Pass0 绘制全景CubeMap的天盒
        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            ENDCG
            
        }
        //Pass1 绘制光源图标
        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag1
            ENDCG
            
        }
        //Pass2 绘制6图天盒
        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag2
            ENDCG
            
        }
    }
}