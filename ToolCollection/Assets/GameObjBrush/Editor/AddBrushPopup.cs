﻿//*****************************************************************************
//Created By ZJ on 2018年12月28日.
//
//@Description 添加画窗口
//*****************************************************************************
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace JZYX.GameObjectBrush
{
    public class AddBrushPopup : EditorWindow
    {
        private List<BrushObj> _brushL;
        private GameObject _targGameObj;
        private Color _redColor = new Color(1, 0.45f, 0.45f);
        private Color _greenColor = new Color(0.47f, 1f, 0.45f);

        public static void OpenWin()
        {
            EditorWindow win = GetWindow<AddBrushPopup>();
            win.maxSize = new Vector2(300, 100);
            win.minSize = new Vector2(300, 100);
            win.Show();
        }

        public void OnGUI()
        {

            GUILayout.Label("新建一个画笔");
            GUILayout.BeginHorizontal();
            GUILayout.Label("目标预制体: ");
            _targGameObj = (GameObject) EditorGUILayout.ObjectField(_targGameObj, typeof(GameObject), false);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUI.backgroundColor = _redColor;
            if (GUILayout.Button("取消"))
            {
                this.Close();
            }

            GUI.backgroundColor = _greenColor;
            if (GUILayout.Button("创建"))
            {
                if (_targGameObj)
                {
                    GameObjBrushEditor.AddBrush(new BrushObj(_targGameObj));
                }

                this.Close();
                GameObjBrushEditor.Refresh();

            }

            GUILayout.EndHorizontal();
        }
    }
}