﻿//*****************************************************************************
//Created By ZJ on 2018年12月27日.
//
//@Description GameObj画刷主窗口
//*****************************************************************************

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Random = UnityEngine.Random;

namespace JZYX.GameObjectBrush
{
    public class GameObjBrushEditor : EditorWindow
    {
        #region 局部变量

        private bool _onEdit = false; //是否正在编辑模式
        private int _nowBrushSetsIdx = 0; //当前选择笔刷集索引
        private BrushSets _nowBrushSets = null; //当前的画笔集
        private Color _oldColor; //GUI背景色缓存
        private Color _redColor = new Color(1, 0.45f, 0.45f); //红色GUI背景色定义
        private Color _greenColor = new Color(0.47f, 1f, 0.45f); //绿色背景色定义
        private bool _pause = false; //是否正在编辑模式

        //空间区块字典(缩小密度检查时的遍历数量)
        private Dictionary<Vector3, Dictionary<GameObject, Vector3>> _spaceGridDic =
            new Dictionary<Vector3, Dictionary<GameObject, Vector3>>();

        //刷出GameObj的层级
        private int _targetLayer = 0;

        //当前的主笔刷
        private BrushObj _mainBrush = null;

        //当前的从属笔刷
        private List<BrushObj> _subBrushs = new List<BrushObj>();

        //笔刷ScrollView的位置变量
        private Vector2 _BrushScrollPos = Vector2.zero;

        //笔刷属性ScrollView的位置变量
        private Vector2 _BrushSettingScrollPos = Vector2.zero;

        //父物体transform
        private Transform _targetParent;

        //忽略层级
        private LayerMask _ignorLayerMask = new LayerMask();

        #endregion

        #region 常量

        //保存目标层级设置的key
        private const string TARGET_LAYER_KEY = "GameObjBrush_TargetLayer_Key";

        //保存上次使用画笔集的key
        private const string LAST_SETS_KEY = "GameObjBrush_LastSets_Key";

        //空间网格的边长
        private const int SPACE_GRID_SIZE = 5;

        //画刷按钮尺寸
        private const int BRUSH_BTN_SIZE = 100;

        #endregion

        //窗口单例对象
        public static GameObjBrushEditor Instance;

        /// <summary>
        /// 窗口创建函数
        /// </summary>
        [MenuItem("Tools/GameObjBrush")]
        public static void CreateGameObjBrushWin()
        {
            EditorWindow win = GetWindow<GameObjBrushEditor>("GOBrush EA");

            win.Show();
        }

        #region 外部静态接口

        /// <summary>
        /// 设置当前画刷集索引并更新画刷集信息
        /// </summary>
        /// <param name="value"></param>
        public static void SetBrushSetsIdx(int value)
        {
            if (Instance)
            {
                Instance.UpdateBrushSets(value);
                Instance.Repaint();
            }
        }

        /// <summary>
        /// 更新界面
        /// </summary>
        public static void Refresh()
        {
            if (Instance)
            {
                Instance.Repaint();
            }
        }

        /// <summary>
        /// 添加画刷
        /// </summary>
        /// <param name="brush"></param>
        public static void AddBrush(BrushObj brush)
        {
            if (Instance && Instance._nowBrushSets)
            {
                Instance._nowBrushSets.BrushList.Add(brush);
                Instance._nowBrushSets.Save();
            }
        }

        /// <summary>
        /// 添加画刷图层
        /// </summary>
        /// <param name="tag"></param>
        public static void AddLayer(string tag)
        {
            if (Instance && Instance._nowBrushSets)
            {
                Instance._nowBrushSets.AddLayer(tag);
            }
        }

        #endregion


        #region 生命周期函数

        /// <summary>
        /// 启用
        /// </summary>
        public void OnEnable()
        {
            //记录当前的窗口对象
            Instance = this;
            //添加场景绘制代理
            SceneView.onSceneGUIDelegate += OnSceneGUI;
            //添加撤销代理
            Undo.undoRedoPerformed += UpdateUndo;
            //读取上次选择的目标层级
            if (EditorPrefs.HasKey(TARGET_LAYER_KEY))
            {
                _targetLayer = EditorPrefs.GetInt(TARGET_LAYER_KEY);
            }

            //读取上次选择的画刷集
            if (EditorPrefs.HasKey(LAST_SETS_KEY))
            {
                UpdateBrushSets(EditorPrefs.GetInt(LAST_SETS_KEY));
            }

            BrushSets.BrushSetsList allBrushSetsList = BrushSets.GetAllBrushSets();
            if (allBrushSetsList.BrushSetsL.Count > 0)
            {
                _nowBrushSetsIdx = Mathf.Min(allBrushSetsList.BrushSetsL.Count - 1, _nowBrushSetsIdx);
                _nowBrushSets = allBrushSetsList.BrushSetsL[_nowBrushSetsIdx];
            }
           
            //收集当前层级所有笔刷刷出来的obj
            FindAllObj();
        }

        /// <summary>
        /// 禁用
        /// </summary>
        public void OnDisable()
        {
            //移除代理
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            Undo.undoRedoPerformed -= UpdateUndo;
            //保存当前画刷集信息
            if (_nowBrushSets)
            {
                _nowBrushSets.Save();
            }
            //保存目标层级和上次使用的画刷集信息
            EditorPrefs.SetInt(TARGET_LAYER_KEY, _targetLayer);
            EditorPrefs.SetInt(LAST_SETS_KEY, _nowBrushSetsIdx);
        }

        #endregion

        #region 公有接口

        /// <summary>
        /// 更新当前使用的画刷集
        /// </summary>
        /// <param name="newSetsIdx">要使用的画刷集索引</param>
        public void UpdateBrushSets(int newSetsIdx)
        {
            if (_nowBrushSets)
            {
                _nowBrushSets.Save();
            }

            BrushSets.BrushSetsList allBrushSetsList = BrushSets.GetAllBrushSets();
            if (allBrushSetsList.BrushSetsL.Count <= 0)
            {
                return;
            }

            _nowBrushSetsIdx = newSetsIdx;
            _nowBrushSetsIdx = Mathf.Min(allBrushSetsList.BrushSetsL.Count - 1, _nowBrushSetsIdx);

            _nowBrushSets = allBrushSetsList.BrushSetsL[_nowBrushSetsIdx];
            _mainBrush = null;
            _subBrushs.Clear();
        }

        #endregion


        #region 界面绘制

        public void OnGUI()
        {
            EditorGUILayout.Space();
            _oldColor = GUI.backgroundColor;

            #region 画刷集部分

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("画刷集: ");
            BrushSets.BrushSetsList allBrushSetsList = BrushSets.GetAllBrushSets();
            EditorGUI.BeginChangeCheck();

            var tempBrusSets = EditorGUILayout.Popup(_nowBrushSetsIdx, allBrushSetsList.GetNameL());
            if (GUILayout.Button("+"))
            {
                CreateBrushSetsPopup.OpenWindow();
            }

            if (EditorGUI.EndChangeCheck() || !_nowBrushSets)
            {
                UpdateBrushSets(tempBrusSets);
            }

            EditorGUILayout.EndHorizontal();

            #endregion

            #region 画刷ScrollView

            //ScrollView
            _BrushScrollPos = EditorGUILayout.BeginScrollView(_BrushScrollPos);
            if (_nowBrushSets)
            {
                int column = Mathf.FloorToInt(this.position.width / (BRUSH_BTN_SIZE + 10));
                for (int i = 0; i <= _nowBrushSets.BrushList.Count; i++)
                {
                    if (i % column == 0)
                    {
                        EditorGUILayout.BeginHorizontal();
                    }

                    if (i == _nowBrushSets.BrushList.Count)
                    {
                        GUI.backgroundColor = _oldColor;
                        if (GUILayout.Button("+", GUILayout.Width(BRUSH_BTN_SIZE), GUILayout.Height(BRUSH_BTN_SIZE)))
                        {
                            AddBrushPopup.OpenWin();
                        }
                    }
                    else
                    {
                        BrushObj brushObj = _nowBrushSets.BrushList[i];
                        GUIContent guiCon = new GUIContent("Missing");
                        if (brushObj.BaseObj)
                        {
                            guiCon =
                                new GUIContent(AssetPreview.GetAssetPreview(brushObj.BaseObj), brushObj.BaseObj.name);
                        }

                        if (_mainBrush == brushObj)
                        {
                            GUI.backgroundColor = Color.blue;
                            if (GUILayout.Button(guiCon, GUILayout.Width(BRUSH_BTN_SIZE),
                                GUILayout.Height(BRUSH_BTN_SIZE)))
                            {
                                if (!Event.current.control)
                                {
                                    _mainBrush = brushObj;
                                    _subBrushs.Clear();
                                }
                            }
                        }
                        else if (_subBrushs.Contains(brushObj))
                        {
                            GUI.backgroundColor = Color.cyan;
                            if (GUILayout.Button(guiCon, GUILayout.Width(BRUSH_BTN_SIZE),
                                GUILayout.Height(BRUSH_BTN_SIZE)))
                            {
                                if (Event.current.control)
                                {
                                    _subBrushs.Remove(brushObj);
                                }
                                else
                                {
                                    _mainBrush = brushObj;
                                    _subBrushs.Clear();
                                }
                            }
                        }
                        else
                        {
                            GUI.backgroundColor = _oldColor;
                            if (GUILayout.Button(guiCon, GUILayout.Width(BRUSH_BTN_SIZE),
                                GUILayout.Height(BRUSH_BTN_SIZE)))
                            {
                                if (Event.current.control)
                                {
                                    if (_mainBrush == null)
                                    {
                                        _mainBrush = brushObj;
                                    }
                                    else
                                    {
                                        _subBrushs.Add(brushObj);
                                    }
                                }
                                else
                                {
                                    _mainBrush = brushObj;
                                    _subBrushs.Clear();
                                }
                            }
                        }
                    }

                    if (i % column == (column - 1) || i == _nowBrushSets.BrushList.Count)
                    {
                        EditorGUILayout.EndHorizontal();
                    }
                }
            }

            EditorGUILayout.EndScrollView();

            #endregion

            #region 画刷操作

            //添加画笔
            EditorGUILayout.BeginHorizontal();
            GUI.backgroundColor = _greenColor;
            if (GUILayout.Button("添加笔刷"))
            {
                AddBrushPopup.OpenWin();
            }

            //移除当前画笔
            GUI.backgroundColor = _redColor;
            EditorGUI.BeginDisabledGroup(_nowBrushSets == null || _mainBrush == null);
            if (GUILayout.Button("移除当前选择的笔刷"))
            {
                if (_nowBrushSets != null)
                {
                    List<BrushObj> removeL = new List<BrushObj>();
                    foreach (BrushObj brush in _subBrushs)
                    {
                        removeL.Add(brush);
                    }

                    removeL.Add(_mainBrush);

                    _nowBrushSets.RemoveBrush(removeL);
                    _mainBrush = null;
                    _subBrushs.Clear();
                }
            }

            EditorGUI.EndDisabledGroup();
            //移除全部画笔
            EditorGUI.BeginDisabledGroup(_nowBrushSets == null || _nowBrushSets.BrushList.Count <= 0);
            if (GUILayout.Button("移除全部笔刷"))
            {
                if (_nowBrushSets != null)
                {
                    _nowBrushSets.RemoveAllBrushs();
                }
            }

            EditorGUI.EndDisabledGroup();


            EditorGUILayout.EndHorizontal();

            #endregion

            #region 画刷图层

            EditorGUILayout.Space();
            GUI.backgroundColor = _oldColor;
            GUILayout.Label("画刷图层:");
            if (_nowBrushSets != null)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUI.BeginChangeCheck();
                _nowBrushSets.SelectBrushLayer =
                    EditorGUILayout.Popup(_nowBrushSets.SelectBrushLayer, _nowBrushSets.TagL.ToArray());
                if (EditorGUI.EndChangeCheck())
                {
                    FindAllObj();
                }

                if (GUILayout.Button("添加笔刷层级"))
                {
                    LayerAddPopup.OpenWin();
                }

                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();
                GUI.backgroundColor = _redColor;
                if (GUILayout.Button("移除图层[" + _nowBrushSets.GetNowLayer() + "]的全部[" + GetObjNum() + "]个物体",
                    GUILayout.Height(40)))
                {
                    foreach (KeyValuePair<Vector3, Dictionary<GameObject, Vector3>> keyValuePair in _spaceGridDic)
                    {
                        foreach (KeyValuePair<GameObject, Vector3> kv in keyValuePair.Value)
                        {
                            DestroyImmediate(kv.Key);
                        }
                    }


                    _spaceGridDic.Clear();
                }
            }

         

            EditorGUILayout.Space();

            #endregion


            #region 编辑操作

            //开始/停止编辑
            if (_onEdit)
            {
                GUI.backgroundColor = _redColor;
                if (GUILayout.Button("停止编辑", GUILayout.Height(50)))
                {
                    SetEditStatus(false);
                }
            }
            else
            {
                GUI.backgroundColor = _greenColor;
                if (GUILayout.Button("开始编辑", GUILayout.Height(50)))
                {
                    SetEditStatus(true);
                }
            }

            //目标层级
            GUI.backgroundColor = _oldColor;
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("目标层级: ");
            EditorGUI.BeginChangeCheck();
            _targetLayer = EditorGUILayout.LayerField(_targetLayer);
            if (EditorGUI.EndChangeCheck())
            {
                FindAllObj();
            }

            EditorGUILayout.EndHorizontal();
            //忽略层级
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("忽略层级: ");
            _ignorLayerMask.value = InternalEditorUtility.ConcatenatedLayersMaskToLayerMask(
                EditorGUILayout.MaskField(
                    InternalEditorUtility.LayerMaskToConcatenatedLayersMask(_ignorLayerMask.value),
                    InternalEditorUtility.layers));
            EditorGUILayout.EndHorizontal();

            //父物体设置
            _targetParent = (Transform) EditorGUILayout.ObjectField("目标父物体:", _targetParent, typeof(Transform), true);
            EditorGUILayout.Space();

            #endregion

            #region 画笔属性

            if (_nowBrushSets != null && _mainBrush != null)
            {
                float tempValue = 0;
                GUILayout.Label("画笔属性:");
                _BrushSettingScrollPos = EditorGUILayout.BeginScrollView(_BrushSettingScrollPos);
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField("关联预制体: ", _mainBrush.BaseObj, typeof(GameObject), false);
                EditorGUI.EndDisabledGroup();
                tempValue = EditorGUILayout.FloatField("笔刷大小: ", _mainBrush.Size);
                if (tempValue < 0)
                {
                    tempValue = 0;
                }

                _mainBrush.Size = tempValue;

                tempValue = EditorGUILayout.FloatField("笔刷密度: ", _mainBrush.Density);
                if (tempValue < 0)
                {
                    tempValue = 0;
                }

                _mainBrush.Density = tempValue;

                _mainBrush.UseClone = EditorGUILayout.Toggle("创建克隆体: ", _mainBrush.UseClone);

                EditorGUILayout.Space();
                GUILayout.Label("缩放范围设置:");
                EditorGUILayout.BeginHorizontal();
                _mainBrush.MinSacleRange = Mathf.Max(EditorGUILayout.FloatField("最小范围:", _mainBrush.MinSacleRange), 0);
                _mainBrush.MaxSacleRange = Mathf.Max(EditorGUILayout.FloatField("最大范围:", _mainBrush.MaxSacleRange),
                    _mainBrush.MinSacleRange);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.MinMaxSlider("缩放范围", ref _mainBrush.MinSacle, ref _mainBrush.MaxSacle,
                    _mainBrush.MinSacleRange, _mainBrush.MaxSacleRange);
                _mainBrush.MinSacle = EditorGUILayout.FloatField(_mainBrush.MinSacle, GUILayout.Width(50));
                _mainBrush.MaxSacle = EditorGUILayout.FloatField(_mainBrush.MaxSacle, GUILayout.Width(50));
                _mainBrush.MinSacle = Mathf.Max(_mainBrush.MinSacle, _mainBrush.MinSacleRange);
                _mainBrush.MaxSacle = Mathf.Min(_mainBrush.MaxSacle, _mainBrush.MaxSacleRange);
                EditorGUILayout.EndHorizontal();


                _mainBrush.CanXRotate = EditorGUILayout.Toggle("启用X轴旋转", _mainBrush.CanXRotate);
                EditorGUILayout.BeginHorizontal();
                if (_mainBrush.CanXRotate)
                {
                    _mainBrush.MinXRotate = EditorGUILayout.FloatField("最小角度", _mainBrush.MinXRotate);
                    _mainBrush.MaxXRotate = EditorGUILayout.FloatField("最大角度", _mainBrush.MaxXRotate);
                }

                EditorGUILayout.EndHorizontal();
                _mainBrush.CanYRotate = EditorGUILayout.Toggle("启用Y轴旋转", _mainBrush.CanYRotate);
                EditorGUILayout.BeginHorizontal();
                if (_mainBrush.CanYRotate)
                {
                    _mainBrush.MinYRotate = EditorGUILayout.FloatField("最小角度", _mainBrush.MinYRotate);
                    _mainBrush.MaxYRotate = EditorGUILayout.FloatField("最大角度", _mainBrush.MaxYRotate);
                }

                EditorGUILayout.EndHorizontal();
                _mainBrush.CanZRotate = EditorGUILayout.Toggle("启用Z轴旋转", _mainBrush.CanZRotate);
                EditorGUILayout.BeginHorizontal();
                if (_mainBrush.CanZRotate)
                {
                    _mainBrush.MinZRotate = EditorGUILayout.FloatField("最小角度", _mainBrush.MinZRotate);
                    _mainBrush.MaxZRotate = EditorGUILayout.FloatField("最大角度", _mainBrush.MaxZRotate);
                }

                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                _mainBrush.AlignSurface = EditorGUILayout.Toggle("对齐表面法线:", _mainBrush.AlignSurface);
                if (_mainBrush.AlignSurface)
                {
                    _mainBrush.AlignAxis =
                        (BrushObj.AlignAxisEnum) EditorGUILayout.EnumPopup("对齐轴:", _mainBrush.AlignAxis);
                }

                EditorGUILayout.EndHorizontal();
                EditorGUILayout.EndScrollView();
            }

            #endregion

            if (GUI.changed)
            {
                Repaint();
            }
        }

        #endregion


        #region 场景绘制

        public void OnSceneGUI(SceneView sceneView)
        {
            if (_onEdit == false)
            {
                return;
            }


            _pause = Event.current.alt;

            if (_pause)
            {
                return;
            }

            if (_mainBrush == null)
            {
                return;
            }


            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~(_ignorLayerMask | 1 << _targetLayer)))
            {
                Handles.color = new Color(20 / 255.0f, 190 / 255.0f, 219 / 255.0f, _mainBrush.Density / 50);
                Handles.DrawSolidArc(hit.point, hit.normal, Vector3.Cross(hit.normal, ray.direction), 360,
                    _mainBrush.Size);
                Handles.color = new Color(20 / 255.0f, 190 / 255.0f, 219 / 255.0f, 1);
                Handles.DrawWireArc(hit.point, hit.normal, Vector3.Cross(hit.normal, ray.direction), 360,
                    _mainBrush.Size);
                Handles.color = new Color(1, 0, 0);
                Handles.DrawLine(hit.point, hit.point + hit.normal * 0.2f);
                if (Event.current.control)
                {
                    if (Event.current.rawType == EventType.ScrollWheel)
                    {
                        _mainBrush.Size -= Event.current.delta.y / 10;
                        _mainBrush.Size = Mathf.Max(0, _mainBrush.Size);
                        Repaint();
                        Event.current.Use();
                        GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    }
                }

                if (Event.current.shift)
                {
                    if (Event.current.rawType == EventType.ScrollWheel)
                    {
                        _mainBrush.Density -= Event.current.delta.y / 5;
                        _mainBrush.Density = Mathf.Max(0, _mainBrush.Density);
                        Repaint();
                        Event.current.Use();
                        GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    }
                }

                if (Tools.current != Tool.View)
                {
                    if (Event.current.rawType == EventType.MouseDown)
                    {
                        if (Event.current.button == 0)
                        {
                            PlaceGameObj(hit.point, hit.normal, ray.direction);
                            Event.current.Use();
                            GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                        }
                        else if (Event.current.button == 1)
                        {
                            RemoveGameObj(hit.point, hit.normal, ray.direction);
                            Event.current.Use();
                            GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                        }
                    }

                    if (Event.current.rawType == EventType.MouseDrag)
                    {
                        if (Event.current.button == 0)
                        {
                            PlaceGameObj(hit.point, hit.normal, ray.direction);
                            Event.current.Use();
                            GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                        }
                        else if (Event.current.button == 1)
                        {
                            RemoveGameObj(hit.point, hit.normal, ray.direction);
                            Event.current.Use();
                            GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                        }
                    }
                }
            }
        }

        #endregion


        #region 私有函数

        /// <summary>
        /// 撤销时更新信息
        /// </summary>
        private void UpdateUndo()
        {
            FindAllObj();
        }

        /// <summary>
        /// 移除物体
        /// </summary>
        /// <param name="hitPoint">射线碰撞点</param>
        /// <param name="hitNormal">碰撞点法线</param>
        /// <param name="rayDir">射线方向</param>
        private void RemoveGameObj(Vector3 hitPoint, Vector3 hitNormal, Vector3 rayDir)
        {
            if (_nowBrushSets)
            {
                Vector3 yAxis = hitNormal.normalized;
                float ydotx = Vector3.Dot(yAxis, Vector3.right);
                float ydoty = Vector3.Dot(yAxis, Vector3.up);
                float ydotz = Vector3.Dot(yAxis, Vector3.forward);


                Matrix4x4 mT = Matrix4x4.TRS(hitPoint,
                    Quaternion.Euler(new Vector3(Mathf.Acos(ydotx) * Mathf.Rad2Deg, Mathf.Acos(ydoty) * Mathf.Rad2Deg,
                        Mathf.Acos(ydotz) * Mathf.Rad2Deg)), Vector3.one);

                BrushObj brush = _mainBrush;

                if (brush != null)
                {
                    int xMax = Mathf.FloorToInt((hitPoint.x + brush.Size) / SPACE_GRID_SIZE);
                    int xMin = Mathf.FloorToInt((hitPoint.x - brush.Size) / SPACE_GRID_SIZE);
                    int yMax = Mathf.FloorToInt((hitPoint.y + brush.Size) / SPACE_GRID_SIZE);
                    int yMin = Mathf.FloorToInt((hitPoint.y - brush.Size) / SPACE_GRID_SIZE);
                    int zMax = Mathf.FloorToInt((hitPoint.z + brush.Size) / SPACE_GRID_SIZE);
                    int zMin = Mathf.FloorToInt((hitPoint.z - brush.Size) / SPACE_GRID_SIZE);
                    for (int i = xMin; i <= xMax; i++)
                    {
                        for (int j = yMin; j <= yMax; j++)
                        {
                            for (int k = zMin; k <= zMax; k++)
                            {
                                Vector3 key = new Vector3(i, j, k);
                                if (!_spaceGridDic.ContainsKey(key))
                                {
                                    continue;
                                }


                                List<GameObject> removeL = new List<GameObject>();
                                foreach (KeyValuePair<GameObject, Vector3> keyValuePair in _spaceGridDic[key])
                                {
                                    Vector3 pos1 = mT.inverse.MultiplyPoint(keyValuePair.Value);
                                    if (pos1.magnitude < brush.Size)
                                    {
                                        removeL.Add(keyValuePair.Key);
                                    }
                                }

                                foreach (GameObject gameObject in removeL)
                                {
                                    _spaceGridDic[key].Remove(gameObject);
                                    DestroyImmediate(gameObject);
                                }

                                removeL.Clear();
                            }
                        }
                    }

                    Repaint();
                }
            }
        }

        /// <summary>
        /// 摆放物体
        /// </summary>
        /// <param name="hitPoint">射线碰撞点</param>
        /// <param name="hitNormal">碰撞点法线</param>
        /// <param name="rayDir">射线方向</param>
        private void PlaceGameObj(Vector3 hitPoint, Vector3 hitNormal, Vector3 rayDir)
        {
            if (_nowBrushSets)
            {
                BrushObj brush = _mainBrush;
                if (brush != null && brush.BaseObj)
                {
                    if (brush.Density <= 0 || brush.Size <= 0)
                    {
                        return;
                    }

                    int count = Mathf.FloorToInt(brush.Size * brush.Size * brush.Density);
                    if (count < 1)
                    {
                        count = 1;
                    }

                    for (int i = 0; i < count; i++)
                    {
                        Vector3 xAxis = Vector3.Cross(hitNormal, rayDir).normalized;
                        Vector3 yAxis = hitNormal.normalized;
                        Vector3 zAxis = Vector3.Cross(xAxis, yAxis).normalized;
                        float R = Random.Range(0, brush.Size);
                        float theta = Random.Range(0, 2 * Mathf.PI);
                        Vector3 oPos = hitPoint + R * Mathf.Sin(theta) * xAxis + R * Mathf.Cos(theta) * zAxis +
                                       hitNormal;
                        Ray ray = new Ray(oPos, -hitNormal);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~(_ignorLayerMask | 1 << _targetLayer)))
                        {
                            if (!checkRange(brush, hit.point))
                            {
                                continue;
                            }

                            GameObject obj;
                            GameObject baseObj;
                            if (_subBrushs.Count > 0)
                            {
                                int idx = Random.Range(0, _subBrushs.Count + 1);

                                if (idx == 0)
                                {
                                    baseObj = brush.BaseObj;
                                }
                                else
                                {
                                    baseObj = _subBrushs[idx - 1].BaseObj;
                                }
                            }
                            else
                            {
                                baseObj = brush.BaseObj;
                            }

                            if (_mainBrush.UseClone)
                            {
                                obj = Instantiate(baseObj, hit.point, Quaternion.identity);
                            }
                            else
                            {
                                obj = (GameObject) PrefabUtility.InstantiatePrefab(baseObj);
                            }

                            obj.transform.position = hit.point;
                            Undo.RegisterCreatedObjectUndo(obj, "Create By Brush");

                            AddObjToGridDic(obj);
                            obj.transform.localScale =
                                Vector3.one * Random.Range(_mainBrush.MinSacle, _mainBrush.MaxSacle);
                            if (_mainBrush.AlignSurface)
                            {
                                switch (_mainBrush.AlignAxis)
                                {
                                    case BrushObj.AlignAxisEnum.X:
                                        obj.transform.right = hit.normal;
                                        break;
                                    case BrushObj.AlignAxisEnum.Y:
                                        obj.transform.up = hit.normal;
                                        break;
                                    case BrushObj.AlignAxisEnum.Z:
                                        obj.transform.forward = hit.normal;
                                        break;
                                }
                            }


                            float xAngle = Random.Range(_mainBrush.MinXRotate, _mainBrush.MaxXRotate) *
                                           (_mainBrush.CanXRotate ? 1 : 0);
                            float yAngle = Random.Range(_mainBrush.MinXRotate, _mainBrush.MaxXRotate) *
                                           (_mainBrush.CanYRotate ? 1 : 0);
                            float zAngle = Random.Range(_mainBrush.MinXRotate, _mainBrush.MaxXRotate) *
                                           (_mainBrush.CanZRotate ? 1 : 0);
                            obj.transform.Rotate(new Vector3(xAngle, yAngle, zAngle), Space.Self);

                            SetLayer(obj, _targetLayer);
                            string nowTag = _nowBrushSets.GetNowLayer();
                            SetTag(obj, nowTag);
                            if (_targetParent)
                            {
                                obj.transform.SetParent(_targetParent, true);
                                SetStaticFlag(obj, GameObjectUtility.GetStaticEditorFlags(_targetParent.gameObject));
                            }

                            Repaint();
                        }
                    }

                    ;
                }
            }
        }

        /// <summary>
        /// 检查密度
        /// </summary>
        /// <param name="brush">画刷对象</param>
        /// <param name="pos">生成位点</param>
        /// <returns></returns>
        private bool checkRange(BrushObj brush, Vector3 pos)
        {
            float actRange = Mathf.Sqrt(1 / brush.Density) * 2;
            int xMax = Mathf.FloorToInt((pos.x + actRange) / SPACE_GRID_SIZE);
            int xMin = Mathf.FloorToInt((pos.x - actRange) / SPACE_GRID_SIZE);
            int yMax = Mathf.FloorToInt((pos.y + actRange) / SPACE_GRID_SIZE);
            int yMin = Mathf.FloorToInt((pos.y - actRange) / SPACE_GRID_SIZE);
            int zMax = Mathf.FloorToInt((pos.z + actRange) / SPACE_GRID_SIZE);
            int zMin = Mathf.FloorToInt((pos.z - actRange) / SPACE_GRID_SIZE);

            for (int i = xMin; i <= xMax; i++)
            {
                for (int j = yMin; j <= yMax; j++)
                {
                    for (int k = zMin; k <= zMax; k++)
                    {
                        Vector3 key = new Vector3(i, j, k);

                        if (!_spaceGridDic.ContainsKey(key))
                        {
                            continue;
                        }

                        var posL = _spaceGridDic[key].Values;

                        foreach (Vector3 oPos in posL)
                        {
                            if (Vector3.Distance(oPos, pos) <= actRange)
                            {
                                return false;
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 设置编辑状态
        /// </summary>
        /// <param name="status"></param>
        private void SetEditStatus(bool status)
        {
            if (status != _onEdit)
            {
                _onEdit = status;
            }

            if (_onEdit)
            {
                _pause = false;
            }
        }

        /// <summary>
        /// 收集当前画刷图层(tag)所有的GameObj
        /// </summary>
        private void FindAllObj()
        {
            if (_nowBrushSets)
            {
                _spaceGridDic.Clear();
                GameObject[] objWithInLayer = GameObject.FindGameObjectsWithTag(_nowBrushSets.GetNowLayer());
                foreach (GameObject gameObject in objWithInLayer)
                {
                    AddObjToGridDic(gameObject);
                }
            }
        }

        //设置tag
        private void SetTag(GameObject obj, string tag)
        {
            if (!InternalEditorUtility.tags.Contains(tag))
            {
                InternalEditorUtility.AddTag(tag);
            }

            obj.tag = tag;
        }

        //项空间区块字典中添加元素
        private void AddObjToGridDic(GameObject obj)
        {
            int x = Mathf.FloorToInt(obj.transform.position.x / SPACE_GRID_SIZE);
            int y = Mathf.FloorToInt(obj.transform.position.y / SPACE_GRID_SIZE);
            int z = Mathf.FloorToInt(obj.transform.position.z / SPACE_GRID_SIZE);
            Vector3 gridKey = new Vector3(x, y, z);
            if (!_spaceGridDic.ContainsKey(gridKey))
            {
                _spaceGridDic.Add(gridKey, new Dictionary<GameObject, Vector3>());
            }


            _spaceGridDic[gridKey].Add(obj, obj.transform.position);
        }

        //过去当前画刷图层所有的物体数量
        private int GetObjNum()
        {
            int ret = 0;
            foreach (KeyValuePair<Vector3, Dictionary<GameObject, Vector3>> keyValuePair in _spaceGridDic)
            {
                ret += keyValuePair.Value.Count;
            }

            return ret;
        }

        //递归设置layer
        private void SetLayer(GameObject o, int layer)
        {
            int childCount = o.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                SetLayer(o.transform.GetChild(i).gameObject, layer);
            }

            o.layer = layer;
        }

        //递归设置StaticFlag
        private void SetStaticFlag(GameObject o, StaticEditorFlags flags)
        {
            int childCount = o.transform.childCount;
            for (int i = 0; i < childCount; i++)
            {
                SetStaticFlag(o.transform.GetChild(i).gameObject, flags);
            }

            GameObjectUtility.SetStaticEditorFlags(o, flags);
        }

        #endregion
    }
}