﻿//*****************************************************************************
//Created By ZJ on 2019年2月18日.
//
//@Description 阵型生成器主窗口
//*****************************************************************************


using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


namespace Package
{
    [CustomEditor(typeof(FormGenerator))]
    public class FormGeneratorEditor : Editor
    {
        #region 私有变量

        private FormGenerator tar;

        private Material _mat;
        private Material _canvasMat;
        private Material _brushMat;
        private Mesh _canvasMesh;


        private bool _showCanvas = true;
        private Vector2 _boundSize = Vector2.one;
        private Mesh _brushMesh;
        private Transform t;
        private FormGeneratorLib.PathPos selectPathP;
        private Color _defualtGreen = new Color(0.5f, 1, 0.5f, 1);
        private Color _defualtRed = new Color(1f, 0.5f, 0.5f, 1);

        private const int DATA_VER = 1;
        private const string CHECK_STR = "FormEditorSetting";

        #endregion


        private void OnEnable()
        {
            _mat = new Material(Shader.Find("Hidden/FormGeneratorCapture"));
            _canvasMat = new Material(Shader.Find("Hidden/FormGeneratorCanvas"));
            _brushMat = new Material(Shader.Find("Hidden/FormGeneratorBrush"));
            tar = (FormGenerator) target;
            _boundSize = new Vector2(tar.bounds.size.x, tar.bounds.size.z);
            _brushMesh = FormGeneratorLib.GenQuad();
            t = tar.gameObject.transform;
            InitBaseMesh();
        }


        private void OnDestroy()
        {
            if (_canvasMat)
            {
                DestroyImmediate(_canvasMat);
            }

            if (_mat)
            {
                DestroyImmediate(_mat);
            }

            if (_brushMat)
            {
                DestroyImmediate(_brushMat);
            }

            if (_canvasMesh)
            {
                _canvasMesh.Clear();
                DestroyImmediate(_canvasMesh);
            }

            if (_brushMesh)
            {
                _brushMesh.Clear();
                DestroyImmediate(_brushMesh);
            }
        }

        public override void OnInspectorGUI()
        {
            if (tar == null || tar.gameObject == null || !tar.gameObject.activeSelf)
                return;
            if (!tar.enabled)
            {
                return;
            }


            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("加载设置数据"))
            {
                string path = EditorUtility.OpenFilePanel("打开", Application.dataPath, "bytes");
                if (!string.IsNullOrEmpty(path))
                {
                    FileStream file = File.Open(path, FileMode.Open);
                    BinaryReader reader = new BinaryReader(file);

                    LoadSetting(reader, file.Length);
                    file.Close();
                    reader.Close();
                }
            }

            if (GUILayout.Button("保存设置数据"))
            {
                SaveSetting();
            }

            EditorGUILayout.EndHorizontal();


            EditorGUI.BeginChangeCheck();
            _boundSize = EditorGUILayout.Vector2Field("阵型尺寸", _boundSize);
            if (EditorGUI.EndChangeCheck())
            {
                tar.bounds.size = (new Vector3(_boundSize.x, 1, _boundSize.y));
            }

            EditorGUI.BeginChangeCheck();


            _showCanvas = EditorGUILayout.Toggle("显示画布", _showCanvas);
            if (tar.CanvasObj)
            {
                tar.CanvasObj.SetActive(_showCanvas);
            }

            tar.DrawGrid = EditorGUILayout.Toggle("显示网格", tar.DrawGrid);
            EditorGUILayout.Space();
            tar.BaseTex = (Texture2D) EditorGUILayout.ObjectField("预设阵型图", tar.BaseTex, typeof(Texture2D), false);
            if (GUILayout.Button("加载预设阵型图"))
            {
                if (tar.BaseTex != null)
                {
                    Graphics.Blit(tar.BaseTex, tar._rt);
                }
            }

            EditorGUILayout.Space();
            DrawToolBar();
            EditorGUILayout.Space();

            tar.BrushObj.SetActive(tar.DrawType == FormGeneratorLib.DrawType.Brush);

            switch (tar.DrawType)
            {
                case FormGeneratorLib.DrawType.Path:
                    DrawPathTool();
                    break;
                case FormGeneratorLib.DrawType.Brush:
                    DrawBrushTool();
                    break;
                case FormGeneratorLib.DrawType.CenterPos:
                    DrawCenterPosTool();
                    break;
                case FormGeneratorLib.DrawType.LeaderPos:
                    DrawLeaderPosTool();
                    break;
                default:
                    break;
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();


            if (GUILayout.Button("清理阵型图"))
            {
                FormGeneratorLib.ClearRT(tar._rt);
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            GUILayout.Label("数据生成: ");
            GUILayout.Label("当前站位数量: " + tar.BakePosL.Count);
            GUI.backgroundColor = _defualtGreen;
            tar.Des = EditorGUILayout.Slider("密度倍数", tar.Des, 0, 2);
            if (GUILayout.Button("预览站位数据", new[] {GUILayout.Height(40)}))
            {
                RemovePreviewModel();
                List<Vector3> fullList = FormGeneratorLib.BakeFormPos(tar, tar.Arr);
                float S = tar.Arr.x * tar.Arr.y * tar.Des;
                float C = tar.Arr.x / tar.Arr.y;
                float Y = Mathf.Round(Mathf.Sqrt(S / C));
                float X = Mathf.Round(C * Y);
                tar.BakePosL.Clear();
                for (int i = 0; i <= X; i++)
                {
                    for (int j = 0; j <= Y; j++)
                    {
                        float x = tar.Arr.x * i / X;
                        float y = tar.Arr.y * j / Y;
                        Vector3 pos = new Vector3(Mathf.Round(x), 0, Mathf.Round(y));
                        if (fullList.Contains(pos))
                        {
                            tar.BakePosL.Add(new Vector3(x, 0, y));
                        }
                    }
                }
            }

            GUI.backgroundColor = _defualtRed;
            if (tar.BakePosL != null && tar.BakePosL.Count > 0 && GUILayout.Button("清理站位数据"))
            {
                tar.BakePosL.Clear();
            }

            GUI.backgroundColor = Color.white;
            EditorGUILayout.Space();
            GUILayout.Label("预览设置:");

            if (tar.BakePosL.Count > 0)
            {
                tar.PreviewModel =
                    (GameObject) EditorGUILayout.ObjectField("小兵模型:", tar.PreviewModel, typeof(GameObject), true);
                if (tar.PreviewModel)
                {
                    if (GUILayout.Button("加载预览模型"))
                    {
                        RemovePreviewModel();

                        foreach (Vector3 pos in tar.BakePosL)
                        {
                            GameObject temp = (GameObject) PrefabUtility.InstantiatePrefab(tar.PreviewModel);
                            temp.transform.SetParent(tar.PreviewObj.transform);
                            temp.transform.localPosition = tar.Grid2Show(pos, tar.Arr);
                        }
                    }
                }
            }

            if (GUILayout.Button("移除预览模型"))
            {
                RemovePreviewModel();
            }

            EditorGUILayout.Space();
            if (GUILayout.Button("导出站位数据"))
            {
                ExportData();
            }
        }

        private void OnSceneGUI()
        {
            if (tar == null || tar.gameObject == null)
                return;
            if (!tar.enabled)
            {
                return;
            }

            Vector2 spaceUnit = new Vector2(tar.bounds.extents.x * 2 / tar.Arr.x,
                tar.bounds.extents.z * 2 / tar.Arr.y);
            if (!Event.current.alt)
            {
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    switch (tar.DrawType)
                    {
                        case FormGeneratorLib.DrawType.Brush:
                            DealBrushSceneInterface(hit);
                            break;
                        case FormGeneratorLib.DrawType.Path:
                            DealPathSceneInterface(hit);
                            break;
                        case FormGeneratorLib.DrawType.CenterPos:
                            DealCenterSceneInterface(hit);
                            break;
                        case FormGeneratorLib.DrawType.LeaderPos:
                            DealLeaderPosSceneInterface(hit);
                            break;
                        default:
                            break;
                    }
                }
            }

            if (tar.DrawType == FormGeneratorLib.DrawType.Path)
            {
                foreach (FormGeneratorLib.PathPos pp in tar.PathPos)
                {
                    Handles.color = selectPathP == pp ? Color.green : Color.magenta;


                    Handles.SphereHandleCap(
                        0,
                        t.localToWorldMatrix.MultiplyPoint(pp.Pos),
                        Quaternion.identity,
                        spaceUnit.magnitude * 0.5f,
                        EventType.Repaint
                    );
                    Handles.color = Color.green;
                    int idx = tar.PathPos.IndexOf(pp);
                    if (idx > 0)
                    {
                        Handles.DrawLine(t.localToWorldMatrix.MultiplyPoint(pp.Pos),
                            t.localToWorldMatrix.MultiplyPoint(tar.PathPos[idx - 1].Pos));
                    }

                    if (idx == tar.PathPos.Count - 1)
                    {
                        Handles.DrawLine(t.localToWorldMatrix.MultiplyPoint(pp.Pos),
                            t.localToWorldMatrix.MultiplyPoint(tar.PathPos[0].Pos));
                    }
                }
            }


            Handles.color = new Color(1, 1, 1, tar.DrawType == FormGeneratorLib.DrawType.CenterPos ? 1 : 0.5f);
            Handles.SphereHandleCap(
                0,
                t.localToWorldMatrix.MultiplyPoint(tar.Grid2Show(tar.CenterPos, tar.Arr)),
                Quaternion.identity,
                spaceUnit.magnitude * 0.8f,
                EventType.Repaint
            );


            Handles.color = new Color(1, 1, 0, tar.DrawType == FormGeneratorLib.DrawType.LeaderPos ? 1 : 0.5f);
            Handles.SphereHandleCap(
                0,
                t.localToWorldMatrix.MultiplyPoint(tar.Grid2Show(tar.LeaderPos, tar.Arr)),
                Quaternion.identity,
                spaceUnit.magnitude * 0.8f,
                EventType.Repaint
            );
            Handles.color = Color.white;

            if (tar.PreviewObj.transform.GetComponentsInChildren<Transform>().Length <= 1)
            {
                foreach (Vector3 pos in tar.BakePosL)
                {
                    Handles.color = Color.red;

                    Vector3 localPos = tar.Grid2Show(pos, tar.Arr);
                    Handles.SphereHandleCap(
                        0,
                        t.localToWorldMatrix.MultiplyPoint(localPos),
                        Quaternion.identity,
                        spaceUnit.magnitude * 0.5f,
                        EventType.Repaint
                    );
                }
            }

            DrawBaseVert(tar.bounds, t.position);
        }

        #region 私有方法

        private void RemovePreviewModel()
        {
            Transform[] c = tar.PreviewObj.transform.GetComponentsInChildren<Transform>();
            foreach (Transform trans in c)
            {
                if (trans && trans != tar.PreviewObj.transform)
                {
                    DestroyImmediate(trans.gameObject);
                }
            }
        }

        private FormGeneratorLib.PathPos FindSelectPos(Vector3 hitPos)
        {
            FormGeneratorLib.PathPos ret = null;
            Vector2 spaceUnit = new Vector2(tar.bounds.extents.x * 2 / tar.Arr.x,
                tar.bounds.extents.z * 2 / tar.Arr.y);

            float pathPosR = spaceUnit.magnitude * 0.5f;
            if (tar.PathPos.Count > 0)
            {
                float minDis = Mathf.Infinity;
                FormGeneratorLib.PathPos closestPos = null;
                foreach (FormGeneratorLib.PathPos pp in tar.PathPos)
                {
                    float dis = pp.CalcDistance(hitPos);
                    if (dis < minDis)
                    {
                        minDis = dis;
                        closestPos = pp;
                    }
                }


                if (closestPos != null && minDis < pathPosR)
                {
                    ret = closestPos;
                }
            }

            return ret;
        }

        private void DealLeaderPosSceneInterface(RaycastHit hit)
        {
            if (Event.current.rawType == EventType.MouseDown || Event.current.rawType == EventType.MouseDrag)
            {
                if (Event.current.button == 0)
                {
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    Vector3 hitPos = t.worldToLocalMatrix.MultiplyPoint(hit.point);

                    tar.SetLeaderPos(hitPos);

                    if (tar.Centertype == FormGenerator.CenterPosType.Default)
                    {
                        tar.SetCenterPos(hitPos);
                    }
                }
            }
        }

        private void DealCenterSceneInterface(RaycastHit hit)
        {
            if (tar.Centertype == FormGenerator.CenterPosType.Default)
            {
                return;
            }

            if (Event.current.rawType == EventType.MouseDown || Event.current.rawType == EventType.MouseDrag)
            {
                if (Event.current.button == 0)
                {
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    Vector3 hitPos = t.worldToLocalMatrix.MultiplyPoint(hit.point);
                    tar.SetCenterPos(hitPos);
                }
            }
        }

        private void DealPathSceneInterface(RaycastHit hit)
        {
            if (Event.current.rawType == EventType.MouseDown)
            {
                Vector3 hitPos = t.worldToLocalMatrix.MultiplyPoint(hit.point);
                if (Event.current.button == 0)
                {
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    selectPathP = FindSelectPos(hitPos);
                    if (selectPathP == null)
                    {
                        tar.PathPos.Add(new FormGeneratorLib.PathPos(hitPos));
                        Repaint();
                    }
                }

                if (Event.current.button == 1)
                {
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    FormGeneratorLib.PathPos p = FindSelectPos(hitPos);
                    if (p != null)
                    {
                        tar.PathPos.Remove(p);
                        Repaint();
                    }
                }
            }

            if (Event.current.rawType == EventType.MouseDrag)
            {
                if (selectPathP == null)
                {
                    return;
                }

                Vector3 hitPos = t.worldToLocalMatrix.MultiplyPoint(hit.point);
                selectPathP.Pos = hitPos;
            }
        }

        private void DealBrushSceneInterface(RaycastHit hit)
        {
            tar.BrushMeshF.sharedMesh = _brushMesh;
            _brushMat.SetTexture("_MainTex", tar.Brush);
            tar.BrushMeshR.sharedMaterial = _brushMat;
            tar.BrushObj.transform.localPosition =
                tar.transform.worldToLocalMatrix.MultiplyPoint(hit.point);
            if (Event.current.rawType == EventType.MouseDown)
            {
                if (Event.current.button == 0)
                {
                    _mat.SetTexture("_MainTex", tar.Brush);
                    FormGeneratorLib.DrawPolygonToRT(tar._rt, t, tar, _brushMesh, _mat,
                        tar.BrushObj.transform.localPosition);
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                }

                if (Event.current.button == 1)
                {
                    _mat.SetTexture("_MainTex", tar.Brush);
                    FormGeneratorLib.ClearPolygonToRT(tar._rt, t, tar, _brushMesh, _mat,
                        tar.BrushObj.transform.localPosition);
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                }
            }

            if (Event.current.rawType == EventType.MouseDrag)
            {
                if (Event.current.button == 0)
                {
                    _mat.SetTexture("_MainTex", tar.Brush);
                    FormGeneratorLib.DrawPolygonToRT(tar._rt, t, tar, _brushMesh, _mat,
                        tar.BrushObj.transform.localPosition);
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                }

                if (Event.current.button == 1)
                {
                    _mat.SetTexture("_MainTex", tar.Brush);
                    FormGeneratorLib.ClearPolygonToRT(tar._rt, t, tar, _brushMesh, _mat,
                        tar.BrushObj.transform.localPosition);
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                }
            }

            if (Event.current.control)
            {
                if (Event.current.rawType == EventType.ScrollWheel)
                {
                    tar.BrushSize -= Event.current.delta.y / 30 * tar.bounds.extents.magnitude;
                    tar.BrushSize = Mathf.Max(0, tar.BrushSize);
                    Repaint();
                    Event.current.Use();
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                }
            }
        }


        private void DrawBrushTool()
        {
            tar.Brush = (Texture2D) EditorGUILayout.ObjectField("笔刷", tar.Brush, typeof(Texture2D), false);
            tar.BrushSize = EditorGUILayout.FloatField("画刷大小", tar.BrushSize);
            tar.BrushSize = Mathf.Max(0, tar.BrushSize);
            FormGeneratorLib.ChangeQuadSize(tar.BrushSize, tar.BrushSize, _brushMesh);
        }

        private void DrawPathTool()
        {
            if (GUILayout.Button("清除路径"))
            {
                tar.PathPos.Clear();
            }

            if (tar.PathPos.Count >= 3 && GUILayout.Button("填充路径"))
            {
                Vector3[] vertL = new Vector3[tar.PathPos.Count];
                for (int i = 0; i < tar.PathPos.Count; i++)
                {
                    vertL[i] = tar.PathPos[i].Pos;
                }

                _mat.SetTexture("_MainTex", Texture2D.whiteTexture);
                FormGeneratorLib.DrawPolygonToRT(tar._rt, t, tar, vertL, _mat);
                tar.PathPos.Clear();
            }

            if (tar.PathPos.Count >= 3 && GUILayout.Button("裁剪路径"))
            {
                Vector3[] vertL = new Vector3[tar.PathPos.Count];
                for (int i = 0; i < tar.PathPos.Count; i++)
                {
                    vertL[i] = tar.PathPos[i].Pos;
                }

                _mat.SetTexture("_MainTex", Texture2D.whiteTexture);
                FormGeneratorLib.ClearPolygonToRT(tar._rt, t, tar, vertL, _mat);
                tar.PathPos.Clear();
            }
        }

        private void DrawLeaderPosTool()
        {
            Vector2 pos = EditorGUILayout.Vector2Field("将领位点", new Vector2(tar.LeaderPos.x, tar.LeaderPos.z));

            tar.SetLeaderGridPos(pos);
            if (GUILayout.Button("恢复默认"))
            {
                tar.LeaderPos = tar.GetDefaultGridPos();
            }

            if (tar.Centertype == FormGenerator.CenterPosType.Default)
            {
                tar.CenterPos = tar.LeaderPos;
            }
        }

        private void DrawCenterPosTool()
        {
            tar.Centertype = (FormGenerator.CenterPosType) EditorGUILayout.EnumPopup("中心点类型:", tar.Centertype);
            if (tar.Centertype == FormGenerator.CenterPosType.Default)
            {
                tar.CenterPos = tar.LeaderPos;
            }

            EditorGUI.BeginDisabledGroup(tar.Centertype == FormGenerator.CenterPosType.Default);
            Vector2 pos = EditorGUILayout.Vector2Field("将领位点", new Vector2(tar.CenterPos.x, tar.CenterPos.z));
            tar.SetCenterPos(pos);
            if (GUILayout.Button("恢复默认"))
            {
                tar.LeaderPos = tar.GetDefaultGridPos();
            }

            EditorGUI.EndDisabledGroup();
        }

        private void DrawToolBar()
        {
            GUILayout.Label("工具:");
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginDisabledGroup(tar.DrawType == FormGeneratorLib.DrawType.None);
            GUI.color = tar.DrawType == FormGeneratorLib.DrawType.None ? Color.green : Color.white;
            if (GUILayout.Button("选择"))
            {
                tar.DrawType = FormGeneratorLib.DrawType.None;
            }

            EditorGUI.EndDisabledGroup();
            EditorGUI.BeginDisabledGroup(tar.DrawType == FormGeneratorLib.DrawType.Brush);
            GUI.color = tar.DrawType == FormGeneratorLib.DrawType.Brush ? Color.green : Color.white;
            if (GUILayout.Button("笔刷"))
            {
                tar.DrawType = FormGeneratorLib.DrawType.Brush;
            }

            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(tar.DrawType == FormGeneratorLib.DrawType.Path);
            GUI.color = tar.DrawType == FormGeneratorLib.DrawType.Path ? Color.green : Color.white;
            if (GUILayout.Button("路径"))
            {
                tar.DrawType = FormGeneratorLib.DrawType.Path;
            }

            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(tar.DrawType == FormGeneratorLib.DrawType.LeaderPos);
            GUI.color = tar.DrawType == FormGeneratorLib.DrawType.LeaderPos ? Color.green : Color.white;
            if (GUILayout.Button("将领点"))
            {
                tar.DrawType = FormGeneratorLib.DrawType.LeaderPos;
            }

            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(tar.DrawType == FormGeneratorLib.DrawType.CenterPos);
            GUI.color = tar.DrawType == FormGeneratorLib.DrawType.CenterPos ? Color.green : Color.white;
            if (GUILayout.Button("中心点"))
            {
                tar.DrawType = FormGeneratorLib.DrawType.CenterPos;
            }

            EditorGUI.EndDisabledGroup();
            GUI.color = Color.white;
            EditorGUILayout.EndHorizontal();
        }

        private void InitBaseMesh()
        {
            _canvasMesh = FormGeneratorLib.GenQuad();
        }

        private void ExportData()
        {
            FormData d = CreateInstance<FormData>();

            Vector3 cenPos = tar.ChangeGridPosC(tar.CenterPos);
            Vector3 leaderPos = tar.ChangeGridPosC(tar.LeaderPos);
            List<Vector3> bpl = FormGeneratorLib.BakeFormPos(tar, tar.Arr);
            foreach (Vector3 pos in bpl)
            {
                Vector3 p = tar.ChangeGridPosC(pos);
                d.PosL.Add(p - cenPos);
            }

            d.LeaderPos = leaderPos - cenPos;
            d.CenterPos = -cenPos;
            d.Width = tar.bounds.extents.x * 2;
            d.Height = tar.bounds.extents.z * 2;
            Vector2 spaceUnit = new Vector2(tar.bounds.extents.x * 2 / tar.Arr.x,
                tar.bounds.extents.z * 2 / tar.Arr.y);
            d.CellWidth = spaceUnit.x;
            d.CellHeight = spaceUnit.y;
            string savePath = EditorUtility.SaveFilePanel("SaveData", Application.dataPath, "FormData", "asset");
            if (!string.IsNullOrEmpty(savePath))
            {
                savePath.Replace("\\", "/");
                savePath = savePath.Replace(Application.dataPath, "");
                savePath = "Assets" + savePath;
                AssetDatabase.CreateAsset(d, savePath);
                //savePath = savePath.Replace(".asset", "_editor.bytes");

//                FileStream file = File.Open(savePath + name, FileMode.Create);
//                BinaryWriter writer = new BinaryWriter(file);
//                writer.Write(CHECK_STR);
//                writer.Write(DATA_VER);
//                writer.Write((int)tar.bounds.extents.x);
//                writer.Write((int)tar.bounds.extents.z);
//                writer.Write((int)tar.Arr.x);
//                writer.Write((int)tar.Arr.y);
//                writer.Write((int)tar.LeaderPos.x);
//                writer.Write((int)tar.LeaderPos.z);
//                writer.Write((int)tar.CenterPos.x);
//                writer.Write((int)tar.CenterPos.z);
//                writer.Write((int) tar.Centertype);
//                Texture2D tex = new Texture2D(tar._rt.width, tar._rt.height, TextureFormat.ARGB32, false);
//                Graphics.SetRenderTarget(tar._rt);
//                tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
//                byte[] texData = tex.GetRawTextureData();
//                writer.Write(texData.Length);
//                writer.Write(texData);
//                writer.Close();
//                file.Close();
//                AssetDatabase.Refresh();
//                DestroyImmediate(tex);
            }
        }

        private void SaveSetting()
        {
            string savePath = EditorUtility.SaveFilePanel("SaveData", Application.dataPath, "FormData", "bytes");
            if (!string.IsNullOrEmpty(savePath))
            {
                savePath.Replace("\\", "/");
                savePath = savePath.Replace(Application.dataPath, "");
                savePath = "Assets" + savePath;


                FileStream file = File.Open(savePath + name, FileMode.Create);
                BinaryWriter writer = new BinaryWriter(file);
                writer.Write(CHECK_STR);
                writer.Write(DATA_VER);
                writer.Write((int) tar.bounds.extents.x);
                writer.Write((int) tar.bounds.extents.z);
                writer.Write((int) tar.Arr.x);
                writer.Write((int) tar.Arr.y);
                writer.Write((int) tar.LeaderPos.x);
                writer.Write((int) tar.LeaderPos.z);
                writer.Write((int) tar.CenterPos.x);
                writer.Write((int) tar.CenterPos.z);
                writer.Write((int) tar.Centertype);
                Texture2D tex = new Texture2D(tar._rt.width, tar._rt.height, TextureFormat.ARGB32, false);
                Graphics.SetRenderTarget(tar._rt);
                tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
                byte[] texData = tex.GetRawTextureData();
                writer.Write(texData.Length);
                writer.Write(texData);
                writer.Close();
                file.Close();
                AssetDatabase.Refresh();
                DestroyImmediate(tex);
            }
        }

        private void LoadSetting(BinaryReader reader, long length)
        {
            string checkStr = reader.ReadString();
            if (checkStr != CHECK_STR)
            {
                Debug.LogError("数据文件错误!!!");
                return;
            }

            int ver = reader.ReadInt32();
            if (ver != DATA_VER)
            {
                Debug.LogError("数据文件版本错误!!!");
                return;
            }

            int int1 = reader.ReadInt32();
            int int2 = reader.ReadInt32();
            tar.bounds.extents = new Vector3(int1, 1, int2);
            _boundSize = new Vector2(tar.bounds.size.x, tar.bounds.size.z);

            int1 = reader.ReadInt32();
            int2 = reader.ReadInt32();
            tar.SetLeaderGridPos(new Vector2(int1, int2));
            int1 = reader.ReadInt32();
            int2 = reader.ReadInt32();
            tar.SetCenterPos(new Vector2(int1, int2));
            tar.Centertype = (FormGenerator.CenterPosType) reader.ReadInt32();
            Texture2D tex = new Texture2D(tar._rt.width, tar._rt.height, TextureFormat.ARGB32, false);

            int1 = reader.ReadInt32();
            tex.LoadRawTextureData(reader.ReadBytes(int1));
            tex.Apply();
            Graphics.Blit(tex, tar._rt);
            DestroyImmediate(tex);
        }

        private void DrawBaseVert(Bounds b, Vector3 center)
        {
            _canvasMat.SetTexture("_MainTex", tar._rt);
            if (tar.CanvasMeshR == null)
            {
                return;
            }

            tar.CanvasMeshR.sharedMaterial = _canvasMat;
            //更新画布尺寸
            FormGeneratorLib.ChangeQuadSize(b.extents.x * 2, b.extents.z * 2, _canvasMesh);
            tar.CanvasMeshF.sharedMesh = _canvasMesh;
        }

        #endregion
    }
}