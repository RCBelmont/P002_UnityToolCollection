﻿Shader "Hidden/FormGeneratorCapture"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha
        Cull off
        Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            
            
            #include "UnityCG.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                
                float4 vertex: SV_POSITION;
            };
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4x4 _VP;
            inline float4 M2C(in float3 pos)
            {
                return mul(_VP, mul(unity_ObjectToWorld, float4(pos, 1.0)));
            }
            inline float4 M2C(float4 pos)
            {
                return M2C(pos.xyz);
            }
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = M2C(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed r = step(0.5, col.rrr);
                return fixed4(1, 1, 1, r);
            }
            ENDCG
            
        }

         Pass
        {
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            
            
            #include "UnityCG.cginc"
            
            struct appdata
            {
                float4 vertex: POSITION;
                float2 uv: TEXCOORD0;
            };
            
            struct v2f
            {
                float2 uv: TEXCOORD0;
                
                float4 vertex: SV_POSITION;
            };
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4x4 _VP;
            inline float4 M2C(in float3 pos)
            {
                return mul(_VP, mul(unity_ObjectToWorld, float4(pos, 1.0)));
            }
            inline float4 M2C(float4 pos)
            {
                return M2C(pos.xyz);
            }
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = M2C(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                return o;
            }
            
            fixed4 frag(v2f i): SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed r = step(0.5, col.rrr);
                return fixed4(0, 0, 0, r);
            }
            ENDCG
            
        }
    }
}
