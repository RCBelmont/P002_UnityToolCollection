﻿//*****************************************************************************
//Created By ZJ on 2019年2月18日.
//
//@Description 阵型生成器功能库
//*****************************************************************************

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Package
{
    public class FormGeneratorLib : MonoBehaviour
    {
        public class PathPos
        {
            public Vector3 Pos;

            public PathPos(Vector3 p)
            {
                Pos = p;
            }

            public float CalcDistance(Vector3 tarP)
            {
                return Vector3.Distance(tarP, Pos);
            }
        }

        public enum DrawType
        {
            None, //未选择工具
            Brush, //笔刷
            Path, //路径
            LeaderPos, //编辑将领位置
            CenterPos //编辑中心点
        }

        /// <summary>
        /// 生成一个1x1, 在xoz平面的Quad
        /// </summary>
        /// <returns></returns>
        public static Mesh GenQuad()
        {
            Mesh retMesh = new Mesh();

            Vector3[] vertL = new Vector3[]
            {
                new Vector3(-0.5f, 0, 0.5f), new Vector3(-0.5f, 0, -0.5f), new Vector3(0.5f, 0, 0.5f),
                new Vector3(0.5f, 0, -0.5f)
            };
            Vector2[] uv = new Vector2[]
            {
                new Vector2(0, 1),
                new Vector2(0, 0),
                new Vector2(1, 1),
                new Vector2(1, 0)
            };
            List<int> triL = new List<int>();

            triL.Add(0);
            triL.Add(2);
            triL.Add(1);
            triL.Add(3);
            triL.Add(1);
            triL.Add(2);

            retMesh.vertices = vertL;
            retMesh.triangles = triL.ToArray();
            retMesh.uv = uv;
            retMesh.RecalculateBounds();
            retMesh.RecalculateNormals();
            return retMesh;
        }


        public static Mesh GenPolygonMesh(Vector3[] vertL)
        {
            Mesh mesh = new Mesh();
            mesh.Clear();
            Vector2[] PlaneVertL = new Vector2[vertL.Length];
            for (int i = 0; i < vertL.Length; i++)
            {
                PlaneVertL[i] = new Vector2(vertL[i].x, vertL[i].z);
            }

            int[] triL;
            Triangulator tri = new Triangulator(PlaneVertL);
            triL = tri.Triangulate();
            mesh.vertices = vertL;
            mesh.triangles = triL;
            return mesh;
        }

        public static void CreateCaptureCam(Transform parent, Bounds size, out GameObject go, out Camera cam)
        {
            go = new GameObject();
            cam = go.AddComponent<Camera>();
            cam.aspect = size.extents.x / size.extents.z;
            cam.orthographic = true;
            cam.orthographicSize = size.extents.z;
            go.transform.SetParent(parent, false);
            go.transform.position = Vector3.up * 10;
            go.transform.rotation = Quaternion.Euler(90, 0, 0);
        }

        public static void DrawPolygonToRT(RenderTexture rt, Transform parent, FormGenerator tar, Vector3[] vertL,
            Material mat)
        {
            Mesh mesh = GenPolygonMesh(vertL);
            Camera c;
            GameObject captureGo;
            CreateCaptureCam(parent, tar.bounds, out captureGo, out c);
            CommandBuffer cmd = new CommandBuffer();
            Matrix4x4 mv = c.worldToCameraMatrix;
            Matrix4x4 mp = GL.GetGPUProjectionMatrix(c.projectionMatrix, true);
            mat.SetMatrix("_VP", mp * mv);
            cmd.SetRenderTarget(rt);
            cmd.DrawMesh(mesh, Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one), mat, 0, 0);
            Graphics.ExecuteCommandBuffer(cmd);
            DestroyImmediate(captureGo);
            DestroyImmediate(mesh);
        }

        public static void ClearPolygonToRT(RenderTexture rt, Transform parent, FormGenerator tar, Vector3[] vertL,
            Material mat)
        {
            Mesh mesh = GenPolygonMesh(vertL);
            Camera c;
            GameObject captureGo;
            CreateCaptureCam(parent, tar.bounds, out captureGo, out c);
            CommandBuffer cmd = new CommandBuffer();
            Matrix4x4 mv = c.worldToCameraMatrix;
            Matrix4x4 mp = GL.GetGPUProjectionMatrix(c.projectionMatrix, true);
            mat.SetMatrix("_VP", mp * mv);
            cmd.SetRenderTarget(rt);
            cmd.DrawMesh(mesh, Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one), mat, 0, 1);
            Graphics.ExecuteCommandBuffer(cmd);
            DestroyImmediate(captureGo);
            DestroyImmediate(mesh);
        }

        public static void DrawPolygonToRT(RenderTexture rt, Transform parent, FormGenerator tar, Mesh m,
            Material mat, Vector3 offset)
        {
            Camera c;
            GameObject captureGo;
            CreateCaptureCam(parent, tar.bounds, out captureGo, out c);
            CommandBuffer cmd = new CommandBuffer();
            Matrix4x4 mv = c.worldToCameraMatrix;
            Matrix4x4 mp = GL.GetGPUProjectionMatrix(c.projectionMatrix, true);
            mat.SetMatrix("_VP", mp * mv);
            cmd.SetRenderTarget(rt);
            cmd.DrawMesh(m, Matrix4x4.TRS(Vector3.zero + offset, Quaternion.identity, Vector3.one), mat, 0, 0);
            Graphics.ExecuteCommandBuffer(cmd);
            DestroyImmediate(captureGo);
        }

        public static void ClearPolygonToRT(RenderTexture rt, Transform parent, FormGenerator tar, Mesh m,
            Material mat, Vector3 offset)
        {
            Camera c;
            GameObject captureGo;
            CreateCaptureCam(parent, tar.bounds, out captureGo, out c);
            CommandBuffer cmd = new CommandBuffer();
            Matrix4x4 mv = c.worldToCameraMatrix;
            Matrix4x4 mp = GL.GetGPUProjectionMatrix(c.projectionMatrix, true);
            mat.SetMatrix("_VP", mp * mv);
            cmd.SetRenderTarget(rt);
            cmd.DrawMesh(m, Matrix4x4.TRS(Vector3.zero + offset, Quaternion.identity, Vector3.one), mat, 0, 1);
            Graphics.ExecuteCommandBuffer(cmd);
            DestroyImmediate(captureGo);
        }

        public static List<Vector3> BakeFormPos(FormGenerator tar, Vector2 slice)
        {
            List<Vector3> bakePosL = new List<Vector3>();
            Texture2D tex = new Texture2D(tar._rt.width, tar._rt.height, TextureFormat.RGB24, false);
            Graphics.SetRenderTarget(tar._rt);
            tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
            Vector2 unit = new Vector2(tar._rt.width / slice.x, tar._rt.height / slice.y);


            for (int i = 0; i < slice.x; i++)
            {
                for (int j = 0; j < slice.y; j++)
                {
                    Vector2[] posL = new Vector2[1];
                    posL[0] = new Vector2(unit.x * i + unit.x / 2, unit.y * j + unit.y / 2);
//                    posL[1] = posL[0] + new Vector2(unit.x / 5, 0);
//                    posL[2] = posL[0] + new Vector2(-unit.x / 5, 0);;
//                    posL[3] = posL[0] + new Vector2(0, unit.y / 5);;
//                    posL[4] = posL[0] + new Vector2(0, unit. / 5);;
//                    
                    int count = 0;
                    foreach (Vector2 pix in posL)
                    {
                        if (tex.GetPixel((int) pix.x, (int) pix.y).r >= 0.1)
                        {
                            count += 1;
                        }
                    }

                    if (count >= 1)
                    {
                        Vector3 pp = new Vector3(i, 0, j);
                        if (bakePosL.IndexOf(pp) == -1 && pp != tar.LeaderPos)
                        {
                            bakePosL.Add(pp);
                        }
                    }
                }
            }

            return bakePosL;
        }

        public static void ClearRT(RenderTexture rt)
        {
            CommandBuffer cmd = new CommandBuffer();
            cmd.SetRenderTarget(rt);
            cmd.ClearRenderTarget(true, true, Color.clear);
            Graphics.ExecuteCommandBuffer(cmd);
        }

        public static void ChangeQuadSize(float width, float height, Mesh quad)
        {
            Vector3[] vertL = quad.vertices;
            if (vertL.Length != 4)
            {
                return;
            }

            float hWidth = width / 2;
            float hHeight = height / 2;
            vertL[0] = new Vector3(-hWidth, 0, hHeight);
            vertL[1] = new Vector3(-hWidth, 0, -hHeight);
            vertL[2] = new Vector3(hWidth, 0, hHeight);
            vertL[3] = new Vector3(hWidth, 0, -hHeight);
            quad.vertices = vertL;
            quad.RecalculateBounds();
            quad.RecalculateNormals();
        }
    }
}