﻿//*****************************************************************************
//Created By ZJ on 2019年2月18日.
//
//@Description 阵型生成器脚本
//*****************************************************************************


using System;
using System.Collections.Generic;
using UnityEngine;

namespace Package
{
    [ExecuteAlways]
    [RequireComponent(typeof(BoxCollider))]
    public class FormGenerator : MonoBehaviour
    {
        #region 公有变量

        public Bounds bounds = new Bounds(Vector3.zero, new Vector3(30, 1, 30));
        public Vector2 Arr
        {
            get
            {
                return new Vector2((int)bounds.size.x, (int)bounds.size.z);
            }
        } 
        public bool DrawGrid = true;
        public RenderTexture _rt;
        public Texture2D BaseTex;
        public GameObject CanvasObj;
        public MeshFilter CanvasMeshF;
        public MeshRenderer CanvasMeshR;
        public float Dist = 0;

        public Vector3 LeaderPos = new Vector3(5, 0, Mathf.Floor(10.0f / 4));

        public Vector3 CenterPos = new Vector3(5, 0, Mathf.Floor(10.0f / 4));
        public GameObject PreviewObj;
        public GameObject PreviewModel;
        public List<Vector3> BakePosL = new List<Vector3>();
        public FormGeneratorLib.DrawType DrawType = FormGeneratorLib.DrawType.None;
        public enum CenterPosType
        {
            Default,
            Custom
        }

        public CenterPosType Centertype = CenterPosType.Default;
        public List<FormGeneratorLib.PathPos> PathPos = new List<FormGeneratorLib.PathPos>();
        public float Des = 1;

        //brush相关
        public Texture2D Brush;
        public float BrushSize = 1;
        public GameObject BrushObj;
        public MeshFilter BrushMeshF;
        public MeshRenderer BrushMeshR;

        #endregion

        #region 私有变量

        private BoxCollider _collider;
        private Material _brushMat;

        #endregion

        #region 常量

        private const int RT_WIDTH = 512;
        private const int RT_HEIGHT = 512;

        #endregion

        public Vector3 GetDefaultGridPos()
        {
            return new Vector3(Mathf.Floor(Arr.x / 2), 0, Mathf.Floor(Arr.y / 4 * 3));
        }

        public void SetCenterPos(Vector3 localPos)
        {
            CenterPos = Local2Grid(localPos);
            CenterPos = new Vector3(Mathf.Clamp(CenterPos.x, 0, Arr.x - 1), CenterPos.y,
                Mathf.Clamp(CenterPos.z, 0, Arr.y - 1));
        }

        public void SetCenterPos(Vector2 gridPos)
        {
            CenterPos = new Vector3(Mathf.Floor(gridPos.x), 0, Mathf.Floor(gridPos.y));
            CenterPos = new Vector3(Mathf.Clamp(CenterPos.x, 0, Arr.x - 1), CenterPos.y,
                Mathf.Clamp(CenterPos.z, 0, Arr.y - 1));
        }

        public void SetLeaderPos(Vector3 localPos)
        {
            LeaderPos = Local2Grid(localPos);
            LeaderPos = new Vector3(Mathf.Clamp(LeaderPos.x, 0, Arr.x - 1), LeaderPos.y,
                Mathf.Clamp(LeaderPos.z, 0, Arr.y - 1));
        }

        public void SetLeaderGridPos(Vector2 gridPos)
        {
            LeaderPos = new Vector3(Mathf.Floor(gridPos.x), 0, Mathf.Floor(gridPos.y));
            LeaderPos = new Vector3(Mathf.Clamp(LeaderPos.x, 0, Arr.x - 1), LeaderPos.y,
                Mathf.Clamp(LeaderPos.z, 0, Arr.y - 1));
        }

        public Vector3 ChangeLocalPosLB(Vector3 localPos)
        {
            return localPos + new Vector3(bounds.extents.x, 0, bounds.extents.z);
        }

        public Vector3 ChangeLocalPosC(Vector3 localPos)
        {
            return localPos - new Vector3(bounds.extents.x, 0, bounds.extents.z);
        }

        public Vector3 ChangeGridPosC(Vector3 gridPos)
        {
            return gridPos - new Vector3(Mathf.Floor(Arr.x/2), 0, Mathf.Floor(Arr.y/2));
        }

        public Vector3 Local2Grid(Vector3 localPos)
        {
            Vector2 spaceUnit = new Vector2(bounds.extents.x * 2 / Arr.x,
                bounds.extents.z * 2 / Arr.y);
            Vector3 lp = ChangeLocalPosLB(localPos);
            return new Vector3(Mathf.Floor((lp.x) / spaceUnit.x), 0,
                Mathf.Floor((lp.z) / spaceUnit.y));
        }

        public Vector3 Grid2Local(Vector3 gridPos)
        {
            Vector2 spaceUnit = new Vector2(bounds.extents.x * 2 / Arr.x,
                bounds.extents.z * 2 / Arr.y);
            return ChangeLocalPosC(new Vector3(gridPos.x * spaceUnit.x, 0,
                gridPos.z * spaceUnit.y));
        }

        public Vector3 Grid2Show(Vector3 gridPos, Vector2 arr)
        {
            Vector2 spaceUnit = new Vector2(bounds.extents.x * 2 / arr.x,
                bounds.extents.z * 2 / arr.y);

            return ChangeLocalPosC(new Vector3(gridPos.x * spaceUnit.x + spaceUnit.x / 2, 0,
                gridPos.z * spaceUnit.y + spaceUnit.y / 2));
        }

        // Start is called before the first frame update
        private void OnEnable()
        {
            _collider = GetComponent<BoxCollider>();
            _rt = new RenderTexture(RT_WIDTH, RT_HEIGHT, 0, RenderTextureFormat.ARGB32);
            _rt.name = "FormGenerator RT";
            _brushMat = new Material(Shader.Find("Hidden/FormGeneratorBrush"));

            InitMeshDisplay();
        }

        private void OnDisable()
        {
            if (_rt != null)
            {
                _rt.Release();
                DestroyImmediate(_rt);
                if (BrushObj)
                {
                    DestroyImmediate(BrushObj);
                }

                if (CanvasObj)
                {
                    DestroyImmediate(CanvasObj);
                }

                if (_brushMat)
                {
                    DestroyImmediate(_brushMat);
                }
            }
        }

        private void InitMeshDisplay()
        {
            Transform t = this.transform.Find("CanvasObj");

            if (t == null)
            {
                CanvasObj = new GameObject();
                CanvasObj.name = "CanvasObj";
                CanvasObj.transform.SetParent(transform);
                CanvasObj.transform.localPosition = Vector3.zero;
                CanvasObj.transform.localRotation = Quaternion.identity;
                CanvasObj.transform.localScale = Vector3.one;
                CanvasMeshF = CanvasObj.AddComponent<MeshFilter>();
                CanvasMeshR = CanvasObj.AddComponent<MeshRenderer>();
            }
            else
            {
                CanvasObj = t.gameObject;
                CanvasMeshF = CanvasObj.GetComponent<MeshFilter>();
                CanvasMeshR = CanvasObj.GetComponent<MeshRenderer>();
            }


            t = this.transform.Find("BrushObj");

            if (t == null)
            {
                BrushObj = new GameObject();
                BrushObj.name = "BrushObj";
                BrushObj.transform.SetParent(transform);
                BrushObj.transform.localPosition = Vector3.zero;
                BrushObj.transform.localRotation = Quaternion.identity;
                BrushObj.transform.localScale = Vector3.one;
                BrushMeshF = BrushObj.AddComponent<MeshFilter>();
                BrushMeshR = BrushObj.AddComponent<MeshRenderer>();
            }
            else
            {
                BrushObj = this.transform.Find("BrushObj").gameObject;
                BrushMeshF = BrushObj.GetComponent<MeshFilter>();
                BrushMeshR = BrushObj.GetComponent<MeshRenderer>();
            }

            t = this.transform.Find("PreviewObj");
            if (t == null)
            {
                PreviewObj = new GameObject();
                PreviewObj.name = "PreviewObj";
                PreviewObj.transform.SetParent(transform);
                PreviewObj.transform.localPosition = Vector3.zero;
                PreviewObj.transform.localRotation = Quaternion.identity;
                PreviewObj.transform.localScale = Vector3.one;
            }
            else
            {
                PreviewObj = t.gameObject;
            }
        }

        void OnDrawGizmos()
        {
            if (_collider == null)
            {
                _collider = this.GetComponent<BoxCollider>();
            }

            _collider.size = new Vector3(bounds.extents.x * 2, 0.1f, bounds.extents.z * 2);

            if (!DrawGrid)
            {
                return;
            }

            Transform t = transform;
            Gizmos.color = Color.blue;
            for (int i = 0; i < Arr.x + 1; i++)
            {
                Vector3 p1 = new Vector3(t.position.x - bounds.extents.x + i * bounds.extents.x * 2 / Arr.x,
                    t.position.y,
                    t.position.z - bounds.extents.z);
                Vector3 p2 = new Vector3(t.position.x - bounds.extents.x + i * bounds.extents.x * 2 / Arr.x,
                    t.position.y,
                    t.position.z + bounds.extents.z);
                Gizmos.DrawLine(p1, p2);
            }

            for (int i = 0; i < Arr.y + 1; i++)
            {
                Vector3 p1 = new Vector3(t.position.x - bounds.extents.x, t.position.y,
                    t.position.z - bounds.extents.z + i * bounds.extents.z * 2 / Arr.y);
                Vector3 p2 = new Vector3(t.position.x + bounds.extents.x, t.position.y,
                    t.position.z - bounds.extents.z + i * bounds.extents.z * 2 / Arr.y);
                Gizmos.DrawLine(p1, p2);
            }
        }
    }
}