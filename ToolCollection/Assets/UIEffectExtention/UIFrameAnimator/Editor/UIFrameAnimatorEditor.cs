﻿//*****************************************************************************
//Created By ZJ on 2018年11月30日.
//
//@Description UI帧动画管理器编辑器代码
//*****************************************************************************
using UnityEditor;
using UnityEngine;


namespace JZYX.UIEffectExtentions
{
    [CustomEditor(typeof(UIFrameAnimator))]
    public class UIFrameAnimatorEditor : Editor
    {
        private SerializedProperty _mainTex;
        private SerializedProperty _color;
        private SerializedProperty _hCount;
        private SerializedProperty _vCount;
        private SerializedProperty _speed;
        private SerializedProperty _playOnAwake;
        private SerializedProperty _stopOnLastFrame;

        private SerializedProperty _cullMode;
        private SerializedProperty _sBlend;
        private SerializedProperty _dBlend;


        private SerializedProperty _loopCount;
        private SerializedProperty _loopInterval;

        static readonly GUIContent _mainTexC = new GUIContent("FrameSheet", "序列帧图集");
        static readonly GUIContent _colorC = new GUIContent("ColorTint", "颜色偏移");
        static readonly GUIContent _hCountC = new GUIContent("HorizontalCount", "横向帧数");
        static readonly GUIContent _vCountC = new GUIContent("VerticalCount", "纵向帧数");
        static readonly GUIContent _speedC = new GUIContent("FrameRate", "帧率");


        static readonly GUIContent _loopCountC = new GUIContent("LoopCount", "循环次数,0为无限循环");
        static readonly GUIContent _loopIntervalC = new GUIContent("LoopInterval", "循环间隔");
        static readonly GUIContent _playOnAwakeC = new GUIContent("PlayOnAwake", "一开始就播放");
        static readonly GUIContent _stopOnLastFrameC = new GUIContent("StopOnLastFrame", "停止时是否为最后一帧, 否则返回第一帧");

        static readonly GUIContent _cullModeC = new GUIContent("Cull", "剔除模式");
        static readonly GUIContent _sBlendC = new GUIContent("SourceBlend", "源混合算子");
        static readonly GUIContent _dBlendC = new GUIContent("DestBlend", "目标混合算子");




        void OnEnable()
        {
            _mainTex = serializedObject.FindProperty("_mainTex");
            _color = serializedObject.FindProperty("_color");
            _hCount = serializedObject.FindProperty("_hCount");
            _vCount = serializedObject.FindProperty("_vCount");
            _speed = serializedObject.FindProperty("_speed");
            _loopCount = serializedObject.FindProperty("_loopCount");
            _loopInterval = serializedObject.FindProperty("_loopInterval");
            _playOnAwake = serializedObject.FindProperty("_playOnAwake");
            _stopOnLastFrame = serializedObject.FindProperty("_stopOnLastFrame");
            _cullMode = serializedObject.FindProperty("_cullMode");
            _sBlend = serializedObject.FindProperty("_sBlend");
            _dBlend = serializedObject.FindProperty("_dBlend");
        }

        public override void OnInspectorGUI()
        {
            UIFrameAnimator t = (UIFrameAnimator) target;
            serializedObject.Update();
            EditorGUI.BeginDisabledGroup(t.isPlaying);
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_mainTex, _mainTexC);
            EditorGUILayout.PropertyField(_color, _colorC);
            EditorGUILayout.PropertyField(_hCount, _hCountC);
            EditorGUILayout.PropertyField(_vCount, _vCountC);
            EditorGUILayout.PropertyField(_speed, _speedC);
            EditorGUILayout.PropertyField(_loopCount, _loopCountC);
            EditorGUILayout.PropertyField(_loopInterval, _loopIntervalC);
            EditorGUILayout.PropertyField(_playOnAwake, _playOnAwakeC);
            EditorGUILayout.PropertyField(_stopOnLastFrame, _stopOnLastFrameC);
            EditorGUILayout.PropertyField(_cullMode, _cullModeC);
            EditorGUILayout.PropertyField(_sBlend, _sBlendC);
            EditorGUILayout.PropertyField(_dBlend, _dBlendC);
            EditorGUI.EndDisabledGroup();
            serializedObject.ApplyModifiedProperties();
            if (EditorGUI.EndChangeCheck())
            {
                t.UpdateMat();
            }
        }

        public void OnSceneGUI()
        {
            UIFrameAnimator t = (UIFrameAnimator) target;
            Handles.BeginGUI();
            GUI.Box(new Rect(Screen.width - 205, Screen.height - 175, 200, 170), "FrameAnimate");
            GUILayout.BeginArea(new Rect(Screen.width - 205, Screen.height - 155, 200, 150));
            if (GUILayout.Button("Play"))
            {
                t.Play();
                Repaint();
            }

            if (GUILayout.Button("Pause"))
            {
                t.Pause();
            }

            if (GUILayout.Button("Stop"))
            { 
                t.Stop(_stopOnLastFrame.boolValue);
                Repaint();
            }

            GUILayout.EndArea();
            Handles.EndGUI();
            Repaint();
        }
    }
}