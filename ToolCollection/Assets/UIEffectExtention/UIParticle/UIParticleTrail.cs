﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JZYX.UIEffectExtentions
{
    public class UIParticleTrail : MaskableGraphic
    {
        static readonly List<Vector3> s_Vertices = new List<Vector3>(); //存储粒子系统烘焙mesh
        [HideInInspector] public ParticleSystem _particleSystem; //粒子系统
        [HideInInspector] public ParticleSystemRenderer _particleRenderer; //粒子系统Renderer

        private Mesh _particleMesh; //存储粒子系统烘焙mesh

        //缓存Shader属性ID
        static readonly int _IdMainTex = Shader.PropertyToID("_MainTex");

        public override Material GetModifiedMaterial(Material baseMaterial)
        {
            return base.GetModifiedMaterial(material != null ? material : baseMaterial);
        }

        protected override void OnEnable()
        {
            _particleMesh = new Mesh();
            _particleMesh.MarkDynamic();
            base.OnEnable();
            Canvas.willRenderCanvases += UpdateMesh;
        }

        protected override void OnDisable()
        {
            Canvas.willRenderCanvases -= UpdateMesh;
            DestroyImmediate(_particleMesh);
            _particleMesh = null;
            base.OnDisable();
        }

        void UpdateMesh()
        {
            if (_particleSystem)
            {
                Camera cam;
                if (canvas != null && canvas.worldCamera)
                {
                    cam = canvas.worldCamera;
                }
                else
                {
                    cam = Camera.main;
                }

                bool useTransform = false;
                if (cam != null)
                {
                    Matrix4x4 matrix = default(Matrix4x4);
                    switch (_particleSystem.main.simulationSpace)
                    {
                        case ParticleSystemSimulationSpace.Local:
                            matrix =
                                Matrix4x4.Rotate(_particleSystem.transform.rotation).inverse
                                * Matrix4x4.Scale(_particleSystem.transform.lossyScale).inverse;
                            useTransform = true;
                            break;
                        case ParticleSystemSimulationSpace.World:
                            matrix = _particleSystem.transform.worldToLocalMatrix;
                            useTransform = true;
                            break;
                        case ParticleSystemSimulationSpace.Custom:
                            break;
                    }

                    if (_particleMesh == null)
                    {
                        _particleMesh = new Mesh();
                        _particleMesh.MarkDynamic();
                    }

                    _particleMesh.Clear();

                    _particleRenderer.BakeTrailsMesh(_particleMesh, cam, useTransform);
                    _particleMesh.GetVertices(s_Vertices);
                    var count = s_Vertices.Count;
                    for (int i = 0; i < count; i++)
                    {
                        s_Vertices[i] = matrix.MultiplyPoint3x4(s_Vertices[i]);
                    }

                    _particleMesh.SetVertices(s_Vertices);
                    s_Vertices.Clear();
                }


                canvasRenderer.SetMesh(_particleMesh);
                canvasRenderer.SetTexture(mainTexture);
            }
        }

        //主纹理
        public override Texture mainTexture
        {
            get
            {
                if (material != null && material.HasProperty(_IdMainTex))
                {
                    return material.mainTexture ?? s_WhiteTexture;
                }

                return s_WhiteTexture;
            }
        }
    }
}