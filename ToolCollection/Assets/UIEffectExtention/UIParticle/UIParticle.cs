﻿//*****************************************************************************
//Created By ZJ on 2018年11月23日.
//
//@Description UI粒子
//*****************************************************************************
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;

namespace JZYX.UIEffectExtentions
{
    [RequireComponent(typeof(ParticleSystem))]
    [ExecuteInEditMode]
    public class UIParticle : MaskableGraphic
    {
        private ParticleSystem _particleSystem; //粒子系统
        private ParticleSystemRenderer _particleRenderer; //粒子系统Renderer
        private Mesh _particleMesh; //存储粒子系统烘焙mesh
        static readonly List<Vector3> s_Vertices = new List<Vector3>();//顶点数组, 用于对烘焙mesh进行顶点变换

        [SerializeField] GameObject _trailGameObj; //拖尾GameObj
        [HideInInspector] [SerializeField] UIParticleTrail _trailParticle;//粒子拖尾组件
        [SerializeField] Material _material;//粒子材质
        [SerializeField] Material _traMaterial; //拖尾材质
        //缓存Shader属性ID
        static readonly int _IdMainTex = Shader.PropertyToID("_MainTex");

        public void Init()
        {
            _particleRenderer.enabled = false;
            raycastTarget = false;
        }

        public override Material GetModifiedMaterial(Material baseMaterial)
        {
            return base.GetModifiedMaterial(_material ? _material : baseMaterial);
        }


        protected override void OnEnable()
        {
            _particleSystem = _particleSystem ? _particleSystem : GetComponent<ParticleSystem>();
            _particleRenderer = _particleSystem ? _particleSystem.GetComponent<ParticleSystemRenderer>() : null;
            _particleMesh = new Mesh();
            _particleMesh.MarkDynamic();
            CheckTrail();
            base.OnEnable();
            Canvas.willRenderCanvases += UpdateMesh;
        }

        protected override void OnDisable()
        {
            Canvas.willRenderCanvases -= UpdateMesh;
            DestroyImmediate(_particleMesh);
            _particleMesh = null;
            CheckTrail();
            base.OnDisable();
        }

        //主纹理
        public override Texture mainTexture
        {
            get
            {
                if (_material!= null &&_material.HasProperty(_IdMainTex))
                {
                    return _material.mainTexture ?? s_WhiteTexture;
                }

                return s_WhiteTexture;
            }
        }

        /// <summary>
        /// 更新Mesh
        /// </summary>
        void UpdateMesh()
        {
            if (_particleSystem)
            {
                if (Application.isPlaying)
                {
                    _particleRenderer.enabled = false;
                }

                CheckTrail();
                Camera cam;
                if (canvas != null && canvas.worldCamera)
                {
                    cam = canvas.worldCamera;
                }
                else
                {
                    cam = Camera.main;
                }

                bool useTransform = false;
                if (cam != null)
                {
                    Matrix4x4 matrix = default(Matrix4x4);
                    switch (_particleSystem.main.simulationSpace)
                    {
                        case ParticleSystemSimulationSpace.Local:
                            matrix =
                                Matrix4x4.Rotate(_particleSystem.transform.rotation).inverse
                                * Matrix4x4.Scale(_particleSystem.transform.lossyScale).inverse;
                            useTransform = true;
                            break;
                        case ParticleSystemSimulationSpace.World:
                            matrix = _particleSystem.transform.worldToLocalMatrix;
                            break;
                        case ParticleSystemSimulationSpace.Custom:
                            break;
                    }

                    if (_particleMesh == null)
                    {
                        _particleMesh = new Mesh();
                        _particleMesh.MarkDynamic();
                    }

                    _particleMesh.Clear();
                    if (_particleRenderer.renderMode != ParticleSystemRenderMode.Mesh ||
                        _particleRenderer.meshCount > 0)
                    {
                        _particleRenderer.BakeMesh(_particleMesh, cam, useTransform);
                    }


                    _particleMesh.GetVertices(s_Vertices);
                    var count = s_Vertices.Count;
                    for (int i = 0; i < count; i++)
                    {
                        s_Vertices[i] = matrix.MultiplyPoint3x4(s_Vertices[i]);
                    }

                    _particleMesh.SetVertices(s_Vertices);
                    s_Vertices.Clear();
                }


                canvasRenderer.SetMesh(_particleMesh);
                canvasRenderer.SetTexture(mainTexture);
            }
        }

        /// <summary>
        /// 检查是否包含拖尾
        /// </summary>
        void CheckTrail()
        {
            if (isActiveAndEnabled && _particleSystem && _particleSystem.trails.enabled)
            {
                if (!_trailGameObj)
                {
                    _trailGameObj = new GameObject("[UIParticle] Trail");
                    _trailParticle = _trailGameObj.AddComponent<UIParticleTrail>();
                    var trans = _trailParticle.transform;

                    trans.SetParent(transform);
                    trans.localPosition = Vector3.zero;
                    trans.localRotation = Quaternion.identity;
                    trans.localScale = Vector3.one;

                    _trailParticle.material = _traMaterial;
                    _trailParticle._particleSystem = _particleSystem;
                    _trailParticle._particleRenderer = _particleRenderer;
                }
                else
                {
                   
                    _trailParticle.material = _traMaterial;
                    _trailParticle._particleSystem = _particleSystem;
                    _trailParticle._particleRenderer = _particleRenderer;
                   
                }

                _trailGameObj.SetActive(true);
            }
            else if (_trailGameObj)
            {
                _trailGameObj.SetActive(false);
            }
        }
    }
}