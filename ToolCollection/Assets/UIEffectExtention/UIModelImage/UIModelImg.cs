﻿//*****************************************************************************
//Created By ZJ on 2018年11月28日.
//
//@Description 相机渲染3d模型到RawImage组件
//*****************************************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace JZYX.UIEffectExtentions
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(RawImage))]
    [RequireComponent(typeof(RectTransform))]
    public class UIModelImg : MonoBehaviour
    {
        //buffer设置
        public enum DepthEnum
        {
            NO_BUFFER_0, //无深度buffer
            ONLY_DEPTH_16, //只有深度buffer, 至少16bit
            DEPTH_STENCIL_24 //包含深度buffer和模板buffer, 至少24bit
        }

        //播放状态
        public enum PlayStatus
        {
            STOP, //停止状态
            PAUSE, //暂停状态
            PLAYING //渲染状态
        }

        [SerializeField] private bool _renderOnAwake = true; //是否一开始就运行
        [SerializeField] private Camera _camera; //渲染相机
        [SerializeField] private GameObject _content = null; //渲染对象父级容器,用于整体设置位置
        [SerializeField] private DepthEnum _bufferType = DepthEnum.NO_BUFFER_0; //RenderTexture深度设置
        [SerializeField] private int _rtWidth = 128; //RenderTexture分辨率设置:宽
        [SerializeField] private int _rtHeight = 128; //RenderTexture分辨率设置:高
        [SerializeField] private GameObject _modelContainer = null; //模型容器
        [SerializeField] private GameObject _lightContainer = null; //光照容器


        private PlayStatus playStatus = PlayStatus.STOP;
        private RenderTexture _rt;
        private Vector2 rtSize;
        static private string _layerName = "UIModel";
        private int _layerID = 0;

        void OnEnable()
        {
            int layerId = LayerMask.NameToLayer(_layerName);

            if (layerId == -1)
            {
                Debug.LogError("Please Add Layer Named \"UIModel\"");
            }
            else
            {
                _layerID = layerId;
            }


            Init();
        }

        void OnDisable()
        {
            ReleaseRt();
        }

        // Use this for initialization
        void Start()
        {
            if (_renderOnAwake)
            {
                Play();
            }
            else
            {
                _rt.DiscardContents(true, true);
            }
        }

        // Update is called once per frame
        void Update()
        {
            UpdateRt();
            if (!Application.isPlaying && _camera)
            {
                _camera.Render();
            }
        }

        void OnDestroy()
        {
            ReleaseRt();
        }

        public void OnDrawGizmos()
        {
            if (_modelContainer)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawSphere(_modelContainer.transform.position, 0.5f);
            }

            if (_lightContainer)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(_lightContainer.transform.position, 0.5f);
            }
        }

        /// <summary>
        /// 播放(开始渲染)
        /// </summary>
        public void Play()
        {
            if (playStatus != PlayStatus.PLAYING)
            {
                playStatus = PlayStatus.PLAYING;
                CreateRenderTexture();
                _camera.enabled = true;
                _camera.targetTexture = _rt;
            }
        }

        /// <summary>
        /// 暂停渲染
        /// </summary>
        public void Pause()
        {
            if (playStatus == PlayStatus.PLAYING)
            {
                playStatus = PlayStatus.PAUSE;
                _camera.enabled = false;
            }
        }

        /// <summary>
        /// 停止渲染(清除RenderTexture)
        /// </summary>
        public void Stop()
        {
            if (playStatus != PlayStatus.STOP)
            {
                playStatus = PlayStatus.STOP;
                _camera.enabled = false;
                ReleaseRt();
            }
        }

        /// <summary>
        /// 增加一个显示的模型
        /// </summary>
        /// <param name="obj"></param>
        public void AddObject(GameObject obj)
        {
            AddObject(obj, Vector3.zero, Vector3.zero);
        }

        public void AddObject(GameObject obj, Vector3 localPos, Vector3 localEular)
        {
            ChangeModelLayer(obj);
            obj.transform.SetParent(_modelContainer.transform, false);
            obj.transform.localPosition = localPos;
            obj.transform.localEulerAngles = localEular;
        }

        /// <summary>
        /// 替换显示的模型(清理掉之前所有的显示模型)
        /// </summary>
        /// <param name="obj"></param>
        public void ReplaceObject(GameObject obj)
        {
            ReplaceObject(obj, Vector3.zero, Vector3.zero);
        }

        public void ReplaceObject(GameObject obj, Vector3 localPos, Vector3 localEular)
        {
            ClearObject();
            AddObject(obj, localPos, localEular);
        }

        /// <summary>
        /// 清理所有显示模型
        /// </summary>
        public void ClearObject()
        {
            for (int i = 0; i < _modelContainer.transform.childCount; i++)
            {
                DestroyImmediate(_modelContainer.transform.GetChild(i).gameObject);
            }
        }
        /// <summary>
        /// 快照功能, 在非播放状态调用此接口可进行一次性绘制
        /// </summary>
        public void Snapshoot()
        {
            if (playStatus != PlayStatus.PLAYING)
            {
                if (_rt == null)
                {
                    CreateRenderTexture();
                }
                _camera.targetTexture = _rt;
                _camera.Render();
            }
        }

        /// <summary>
        /// 设置RenderTexture属性
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void SetRenderTextureSize(int width, int height)
        {
            _rtWidth = width;
            _rtHeight = height;
            ReleaseRt();
            CreateRenderTexture();
        }

        //修改显示模型的层级
        private void ChangeModelLayer(GameObject model)
        {
            if (model == null || model.layer == _layerID) return;
            model.layer = _layerID;
            Transform[] trans = model.GetComponentsInChildren<Transform>();
            if (trans == null || trans.Length <= 0) return;
            foreach (Transform form in trans)
            {
                if (form == null) continue;
                form.gameObject.layer = _layerID;
            }
        }

        //初始化(内部函数调用顺序敏感,请勿随意调整顺序)
        private void Init()
        {
            CreateRenderTexture();
            CreateContent();
            CreateSubContainers();
            CreateCamera();
        }

        //创建相机
        private void CreateCamera()
        {
            GameObject camObj = null;
            if (_camera == null)
            {
                Transform camT = this.transform.Find("UIModelCamera");
                if (camT)
                {
                    camObj = camT.gameObject;
                }

                if (camObj == null)
                {
                    camObj = new GameObject();
                    camObj.name = "UIModelCamera";
                    camObj.transform.SetParent(_content.transform, false);
                    _camera = camObj.AddComponent<Camera>();
                    _camera.transform.localPosition = new Vector3(0, 0, -5);
                }
                else
                {
                    _camera = camObj.GetComponent<Camera>();
                }

                _camera.clearFlags = CameraClearFlags.SolidColor;
                _camera.backgroundColor = Color.clear;

                _camera.depth = -100;
            }

            _camera.targetTexture = _rt;
            _camera.gameObject.layer = _layerID;
            _camera.cullingMask = 1 << _layerID;
            _camera.enabled = false;

            UIModelCam t;
            t = _camera.gameObject.GetComponent<UIModelCam>();
            if (t == null)
            {
                t = _camera.gameObject.AddComponent<UIModelCam>();
            }

            t.lightObject = _lightContainer;
            t.content = _content;
        }

        //创建父级容器
        private void CreateContent()
        {
            if (_content == null)
            {
                Transform targetT = this.transform.Find("UIModelContent");
                if (targetT)
                {
                    _content = targetT.gameObject;
                }

                if (_content == null)
                {
                    _content = new GameObject("UIModelContent");
                    _content.transform.SetParent(this.transform, false);
                  
                }

                _content.layer = LayerMask.NameToLayer(_layerName);
            }
        }

        //创建自容器
        private void CreateSubContainers()
        {
            Transform targetT;
            targetT = _content.transform.Find("ModelContainer");
            if (targetT == null)
            {
                _modelContainer = new GameObject("ModelContainer");
                _modelContainer.transform.SetParent(_content.transform, false);
                _modelContainer.gameObject.layer = _layerID;
                _modelContainer.transform.localPosition = new Vector3(0, 0, 5);
            }

            targetT = _content.transform.Find("LightContainer");
            if (targetT == null)
            {
                _lightContainer = new GameObject("LightContainer");
                _lightContainer.transform.SetParent(_content.transform, false);
                _lightContainer.gameObject.layer = _layerID;
            }
        }

        //创建RenderTexture
        private void CreateRenderTexture()
        {
            int depth = GetBufferBit();
            if (_rt == null)
            {
                _rt = new RenderTexture(Mathf.Max(_rtWidth, 1), Mathf.Max(_rtHeight, 1), depth);
                _rt.hideFlags = HideFlags.DontSave;
                _rt.name = "RenderModeld" + _rtWidth + " x " + _rtHeight;
                _rt.dimension = UnityEngine.Rendering.TextureDimension.Tex2D;
                _rt.antiAliasing = 1; //抗锯齿不修改
                _rt.useDynamicScale = false;
                _rt.wrapMode = TextureWrapMode.Clamp;
                _rt.filterMode = FilterMode.Bilinear;
                if (_rt.format != RenderTextureFormat.ARGB32)
                {
                    _rt.format = RenderTextureFormat.ARGB32; //.Default;
                }

                if (_rt.useMipMap != false)
                {
                    _rt.useMipMap = false;
                }
            }

            _rt.depth = depth;
            this.GetComponent<RawImage>().texture = _rt;
        }

        //释放RenderTexture
        private void ReleaseRt()
        {
            if (_camera)
            {
                _camera.targetTexture = null;
            }

            if (_rt)
            {
                _rt.Release();
                DestroyImmediate(_rt);
            }

            _rt = null;
        }

        //更新RenderTextrue
        private void UpdateRt()
        {
            if (_rt)
            {
                bool sizeChange = false;
                bool depthChange = false;
                if ((_rt.width != _rtWidth ||
                     _rt.height != _rtHeight))
                {
                    sizeChange = true;
                }

                if (_rt.depth != GetBufferBit())
                {
                    depthChange = true;
                }

                if (sizeChange || depthChange)
                {
                    if (sizeChange)
                    {
                        ReleaseRt();
                        CreateRenderTexture();
                    }
                    else
                    {
                        ReleaseRt();
                        CreateRenderTexture();
                    }

                    if (_camera)
                    {
                        _camera.targetTexture = _rt;
                    }
                }
            }
        }

        //Depth枚举到buffer数值
        private int GetBufferBit()
        {
            switch (_bufferType)
            {
                case DepthEnum.DEPTH_STENCIL_24:
                    return 24;
                case DepthEnum.ONLY_DEPTH_16:
                    return 16;
                default:
                    return 0;
            }
        }
    }
}