﻿//*****************************************************************************
//Created By ZJ on 2018年11月28日.
//
//@Description 相机渲染3d模型到RawImage的相机组件
//在相机Cull之前将渲染对象整个搬到一个较远的地方进行渲染,避免相互影响, 渲染完成后还原位置
//*****************************************************************************
using UnityEngine;

namespace JZYX.UIEffectExtentions
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class UIModelCam : MonoBehaviour
    {
        public GameObject lightObject; //灯光容器
        public GameObject content;  //父级容器
        private Vector3 oPos; //父级容器初始
        static private Vector3 _renderPos = new Vector3(5000, 5000, 5000); //渲染时父级容器

        // Use this for initialization
        void Start()
        {
            oPos = content.transform.localPosition;
        }


        private void OnPreCull()
        {
            if (content)
            {
                content.transform.position = _renderPos;
            }

            if (lightObject)
            {
                lightObject.SetActive(true);
            }
        }

        void OnPostRender()
        {
            if (content)
            {
                content.transform.localPosition = Vector3.zero;
            }

            if (lightObject)
            {
                lightObject.SetActive(false);
            }
        }
    }
}