﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace JZYX.UIEffectExtentions
{
    [CustomEditor(typeof(UIModelImg))]
    public class UIModelImgEditor : Editor
    {
        private SerializedProperty _renderOnAwake;
        private SerializedProperty _bufferType;
        private SerializedProperty _rtWidth;
        private SerializedProperty _rtHeight;

        private SerializedProperty _camera;
        private SerializedProperty _content;
        private SerializedProperty _modelContainer;
        private SerializedProperty _lightContainer;

        static readonly GUIContent _renderOnAwakeCotnent = new GUIContent("RenderOnAwake", "一开始就启动渲染");
        static readonly GUIContent _bufferTypeCotnent = new GUIContent("BufferType", "RenderTexture缓存类型");
        static readonly GUIContent _rtWidthContent = new GUIContent("Width", "RenderTexture宽");
        static readonly GUIContent _rtHeightContent = new GUIContent("Height", "RenderTexture高");

        static readonly GUIContent _cameraContent = new GUIContent("Camera", "渲染相机");
        static readonly GUIContent _contentContent = new GUIContent("Content", "渲染组件父级容器");
        static readonly GUIContent _modelContainerContent = new GUIContent("ModelContainer", "显示模型容器");
        static readonly GUIContent _lightContainerContent = new GUIContent("LightContainer", "光照容器");


        void OnEnable()
        {
            _renderOnAwake = serializedObject.FindProperty("_renderOnAwake");
            _bufferType = serializedObject.FindProperty("_bufferType");
            _rtWidth = serializedObject.FindProperty("_rtWidth");
            _rtHeight = serializedObject.FindProperty("_rtHeight");
            _camera = serializedObject.FindProperty("_camera");
            _content = serializedObject.FindProperty("_content");
            _modelContainer = serializedObject.FindProperty("_modelContainer");
            _lightContainer = serializedObject.FindProperty("_lightContainer");
        }

        public override void OnInspectorGUI()
        {
            UIModelImg t = (UIModelImg) target;
            serializedObject.Update();
            EditorGUILayout.PropertyField(_renderOnAwake, _renderOnAwakeCotnent);
            EditorGUILayout.PropertyField(_bufferType, _bufferTypeCotnent);
            EditorGUILayout.PropertyField(_rtWidth, _rtWidthContent);
            EditorGUILayout.PropertyField(_rtHeight, _rtHeightContent);
            EditorGUI.BeginDisabledGroup(true);

            EditorGUILayout.PropertyField(_camera, _cameraContent);
            EditorGUILayout.PropertyField(_content, _contentContent);
            EditorGUILayout.PropertyField(_modelContainer, _modelContainerContent);
            EditorGUILayout.PropertyField(_lightContainer, _lightContainerContent);
            EditorGUI.EndDisabledGroup();
            if (GUILayout.Button("Set Image Size"))
            {
                Vector2 size = t.GetComponent<RectTransform>().sizeDelta;
                _rtWidth.intValue = (int)size.x;
                _rtHeight.intValue = (int)size.y;
            }
            serializedObject.ApplyModifiedProperties();

        }
    }
}