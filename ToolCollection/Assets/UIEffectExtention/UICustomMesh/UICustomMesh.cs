﻿//*****************************************************************************
//Created By ZJ on 2018年11月23日.
//
//@Description UI自定义Mesh
//*****************************************************************************

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace JZYX.UIEffectExtentions
{
    public class UICustomMesh : MaskableGraphic
    {
        [SerializeField] private GameObject _target;//被渲染的GameObj
        [SerializeField] private MeshRenderer _mr;//被渲染GameObj的MeshRenderer
        [SerializeField] private MeshFilter _mf;//被渲染GameObj的MeshFielter
        [SerializeField] private SkinnedMeshRenderer _smr;//被渲染GameObj的SkinnedMeshRenderer, 骨骼动画对象具有这个Renderer而没有MeshRenderer和MeshFilter
        [SerializeField] private Mesh _mesh; //接收SkinnedMeshRenderer或者MeshFilter的ShaderdMesh
     
        private Mesh _tempMesh;//接收SkinnedMeshRenderer或者MeshFilter的ShaderdMesh
        private Animator _animator; //Animator组件, 对于帧动画需要修改其cullingMode属性,确保正常update
        private Material[] _matL; //材质列表
        private GameObject _oldTarget; //旧渲染对象缓存

        //外部调用初始化接口, 用于Editor调用创建代码时设置raycastTarget属性
        public void Init(bool sw = false)
        {
            raycastTarget = sw;
        }
        /// <summary>
        /// 外部调用设置对象物体
        /// </summary>
        /// <param name="obj"></param>
        public void SetTargetGameObj(GameObject obj)
        {
            _target = obj;
        }


        protected override void OnEnable()
        {
            base.OnEnable();
            _tempMesh = new Mesh();
            _tempMesh.MarkDynamic();

            //获取Animator并设置cullingMode为AlwaysAnimate, 以便在SkinnedRenderer禁用时仍然能正常
            _animator = this.GetComponent<Animator>();
            if (_animator == null)
            {
                _animator = this.GetComponentInParent<Animator>();
            }
            if (_animator)
            {
                _animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            }

            _target = this.gameObject;
            Canvas.willRenderCanvases += UpdateMesh;
        }

        protected override void OnDisable()
        {
            Canvas.willRenderCanvases -= UpdateMesh;
            base.OnDisable();
            DestroyImmediate(_tempMesh);
            _tempMesh = null;

        }
      

        void UpdateMesh()
        {
            canvasRenderer.Clear();
            if (_target != null && _oldTarget != _target)
            {
                //获取MeshRenderer, 若能获取到则继续获取MeshFilter, 若没有MeshRenderer则继续尝试获取SkinnedMeshRenderer
                _mr = _target.GetComponent<MeshRenderer>();
                if (_mr)
                {
                    _mr.enabled = false;
                    _mf = _target.GetComponent<MeshFilter>();
                    if (_mf)
                    {
                        _mesh = _mf.sharedMesh;
                        _matL = _mr.sharedMaterials;
                    }
                }
                else
                {
                    _smr = _target.GetComponent<SkinnedMeshRenderer>();
                    if (_smr)
                    {
                        _smr.enabled = false;
                        _mesh = _smr.sharedMesh;
                        _matL = _smr.sharedMaterials;
                    }
                }

                _oldTarget = _target;
            }

            if (_smr)
            {
                //由于SkinnedMeshRenderer烘焙网格时会应用animator的缩放,烘焙出的网格若还在animator的子物体中则又会应用一次缩放,因此需要先复位缩放烘焙Mesh后再还原
                Vector3 scale = Vector3.one;
                if (_animator)
                {
                    scale = _animator.transform.localScale;
                    _animator.transform.localScale = Vector3.one;
                }
                _smr.BakeMesh(_tempMesh);
                if (_animator)
                {
                    _animator.transform.localScale = scale;
                }
            }

            if (!_mesh)
            {
                return;
            }

            if (_smr)
            {
                canvasRenderer.SetMesh(_tempMesh);
            }
            else
            {
                canvasRenderer.SetMesh(_mesh);
            }

            if (_matL != null && _matL.Length > 0)
            {
                canvasRenderer.materialCount = _matL.Length;
                for (int i = 0; i < _matL.Length; i++)
                {
                    
                    canvasRenderer.SetMaterial(GetMat(_matL[i]), i);
                }
            }
        }
        //获取材质, 用于获取Mask情况下生成的临时材质
        public Material GetMat(Material baseMaterial)
        {
            return base.GetModifiedMaterial(baseMaterial);
        }
    }
}