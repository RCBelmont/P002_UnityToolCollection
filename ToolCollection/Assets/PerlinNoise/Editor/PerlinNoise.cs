﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class PerlinNoise : EditorWindow
{
    private int picWidth = 256;
    private int picHeight = 256;
    private float xFrequency = 5;
    private float yFrequency = 5;
    [MenuItem("Tools/PerlinNoise")]
    public static void GenPerlinNoise()
    {
        EditorWindow w =  GetWindow<PerlinNoise>();
        w.maxSize = new Vector2(200, 200);
        w.Show();
    }
    

    private void OnGUI()
    {
        picWidth = EditorGUILayout.IntField("TextureWidth", picWidth);
        picWidth = Mathf.Max(1, picWidth);
        EditorGUILayout.Space();
        picHeight = EditorGUILayout.IntField("TextureHeight", picHeight);
        picHeight = Mathf.Max(1, picHeight);
        EditorGUILayout.Space();
        xFrequency = EditorGUILayout.FloatField("Horizontal Frequency", xFrequency);
        EditorGUILayout.Space();
        yFrequency = EditorGUILayout.FloatField("Vertical Frequency", yFrequency);
        EditorGUILayout.Space();
        if (GUILayout.Button("Generate"))
        {
            GenNoise();
        }
    }


    void GenNoise()
    {
        Texture2D t = new Texture2D(picWidth, picHeight);
        for (int i = 0; i < t.width; i++)
        {
            for (int j = 0; j < t.height; j++)
            {
                float s = Mathf.PerlinNoise(xFrequency * i / t.width, yFrequency * j / t.height);
                t.SetPixel(i, j, new Color(s, s, s));
            }
        }

        byte[] buffer = t.EncodeToPNG();
        string savePath = EditorUtility.SaveFilePanel("SavePerlin", "", "PerlinNoise", "png");
        if (!string.IsNullOrEmpty(savePath))
        {
            FileStream s = File.Open(savePath, FileMode.Create);
            s.Write(buffer, 0, buffer.Length);
            s.Close();
        }
    }
}